/**
 * xiegaolei
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}
/* 合法uri*/
export function validateURL(textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母*/
export function validateAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/**
 * validate email
 * @param email
 * @returns {boolean}
 */
export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

/**
 * validate phone
 * @param phone
 * @returns {boolean}
 */
export function validatePhone(phone) {
  const re = /^1\d{10}$/
  return re.test(phone)
}
/**
 * validate qq
 * @param qq
 * @returns {boolean}
 */
export function validateQQ(qq) {
  const re = /^\d{5,12}$/
  return re.test(qq)
}
export const betweenInt = (min, max) => (_rule, v, cb) => {
  const isBetween = v >= min && v <= max
  const isInt = /^[0-9]+$/.test(v)
  if (isBetween && isInt) return cb()
  return cb(new Error(`要求是在${min}到${max}的正整数 [${min}, ${max}]`))
}
export const validateMin = (min) => (_rule, v, cb) => {
  const isMin = v >= min
  const isInt = /^[0-9]+$/.test(v)
  if (isMin && isInt) return cb()
  return cb(new Error(`最小值为${min}整数`))
}
export const validateMax = (max) => (_rule, v, cb) => {
  const isMax = v <= max
  const isInt = /^[0-9]+$/.test(v)
  if (isMax && isInt) return cb()
  return cb(new Error(`最大值为${max}整数`))
}
