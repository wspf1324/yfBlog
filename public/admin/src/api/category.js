import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/category/list',
    method: 'post',
    data: query
  })
}

export function getAll(query) {
  return request({
    url: '/category/getAll',
    method: 'post',
    data: query
  })
}

export function getInfo(id) {
  return request({
    url: '/category/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function save(data) {
  return request({
    url: '/category/save',
    method: 'post',
    data: data
  })
}

export function delAll(data) {
  return request({
    url: '/category/delAll',
    method: 'post',
    data: data
  })
}
export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/category/change',
    method: 'post',
    data: data
  })
}
