import request from '@/utils/request'

export function check(md5) {
  return request({
    url: '/upload/check',
    method: 'post',
    data: { md5: md5 }
  })
}
