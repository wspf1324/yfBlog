import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/roles/list',
    method: 'post',
    data: query
  })
}

export function getAll() {
  return request({
    url: '/roles/getAll',
    method: 'post'
  })
}

export function getInfo(id) {
  return request({
    url: '/roles/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function save(data) {
  return request({
    url: '/roles/save',
    method: 'post',
    data: data
  })
}

export function del(id) {
  return request({
    url: '/roles/del',
    method: 'post',
    data: { id: id }
  })
}

export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/roles/change',
    method: 'post',
    data: data
  })
}
export function delAll(data) {
  return request({
    url: '/roles/delAll',
    method: 'post',
    data: data
  })
}

export function changeAll(data) {
  return request({
    url: '/roles/changeAll',
    method: 'post',
    data: data
  })
}
