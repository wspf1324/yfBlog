import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/user/list',
    method: 'post',
    data: query
  })
}
export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/user/change',
    method: 'post',
    data: data
  })
}
export function getInfo(id) {
  return request({
    url: '/user/getInfo',
    method: 'post',
    data: { id: id }
  })
}
export function saveInfo(data) {
  return request({
    url: '/user/saveInfo',
    method: 'post',
    data: data
  })
}
export function del(id) {
  return request({
    url: '/user/del',
    method: 'post',
    data: { id: id }
  })
}
export function delAll(data) {
  return request({
    url: '/user/delAll',
    method: 'post',
    data: data
  })
}
export function backAll(data) {
  return request({
    url: '/user/backAll',
    method: 'post',
    data: data
  })
}
