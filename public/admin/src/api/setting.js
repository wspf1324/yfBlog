import request from '@/utils/request'

export function getConfig(group) {
  return request({
    url: '/setting/getConfig',
    method: 'post',
    data: { group: group }
  })
}
export function setConfig(data) {
  return request({
    url: '/setting/setConfig',
    method: 'post',
    data: data
  })
}
