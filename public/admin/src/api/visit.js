import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/visit/list',
    method: 'post',
    data: query
  })
}
export function delAll(data) {
  return request({
    url: '/visit/delAll',
    method: 'post',
    data: data
  })
}
export function clear() {
  return request({
    url: '/visit/clear',
    method: 'post',
    data: {}
  })
}

