import request from '@/utils/request'

export function loginByUsername(username, password, verify) {
  const data = {
    userName: username,
    password: password,
    verify: verify
  }

  return request({
    url: '/login/loginIn',
    method: 'post',
    data: data
  })
}

export function verify() {
  return request({
    url: '/login/verify',
    method: 'get'
  })
}

