import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/message/list',
    method: 'post',
    data: query
  })
}
export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/message/change',
    method: 'post',
    data: data
  })
}
export function getInfo(id) {
  return request({
    url: '/message/getInfo',
    method: 'post',
    data: { id: id }
  })
}
export function saveInfo(data) {
  return request({
    url: '/message/saveInfo',
    method: 'post',
    data: data
  })
}
export function del(id) {
  return request({
    url: '/message/del',
    method: 'post',
    data: { id: id }
  })
}
export function delAll(data) {
  return request({
    url: '/message/delAll',
    method: 'post',
    data: data
  })
}
export function backAll(data) {
  return request({
    url: '/message/backAll',
    method: 'post',
    data: data
  })
}
