import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/admin/list',
    method: 'post',
    data: query
  })
}

export function getInfo(id) {
  return request({
    url: '/admin/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function getAdmin() {
  return request({
    url: '/admin/getAdmin',
    method: 'post'
  })
}

export function modify(data) {
  return request({
    url: '/admin/modify',
    method: 'post',
    data: data
  })
}

export function save(data) {
  return request({
    url: '/admin/save',
    method: 'post',
    data: data
  })
}

export function del(id) {
  return request({
    url: '/admin/del',
    method: 'post',
    data: { id: id }
  })
}

export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/admin/change',
    method: 'post',
    data: data
  })
}

export function delAll(data) {
  return request({
    url: '/admin/delAll',
    method: 'post',
    data: data
  })
}

export function changeAll(data) {
  return request({
    url: '/admin/changeAll',
    method: 'post',
    data: data
  })
}
