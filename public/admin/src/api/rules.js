import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/rules/list',
    method: 'post',
    data: query
  })
}

export function getListAll() {
  return request({
    url: '/rules/getLists',
    method: 'post'
  })
}

export function getInfo(id) {
  return request({
    url: '/rules/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function save(data) {
  return request({
    url: '/rules/save',
    method: 'post',
    data: data
  })
}

export function del(id) {
  return request({
    url: '/rules/del',
    method: 'post',
    data: { id: id }
  })
}

export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/rules/change',
    method: 'post',
    data: data
  })
}

export function delAll(data) {
  return request({
    url: '/rules/delAll',
    method: 'post',
    data: data
  })
}

export function changeAll(data) {
  return request({
    url: '/rules/changeAll',
    method: 'post',
    data: data
  })
}
