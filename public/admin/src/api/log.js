import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/log/list',
    method: 'post',
    data: query
  })
}

