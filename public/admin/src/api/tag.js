import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/tag/list',
    method: 'post',
    data: query
  })
}

export function getAll(query) {
  return request({
    url: '/tag/getAll',
    method: 'post',
    data: query
  })
}

export function getInfo(id) {
  return request({
    url: '/tag/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function save(data) {
  return request({
    url: '/tag/save',
    method: 'post',
    data: data
  })
}

export function delAll(data) {
  return request({
    url: '/tag/delAll',
    method: 'post',
    data: data
  })
}
export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/tag/change',
    method: 'post',
    data: data
  })
}
