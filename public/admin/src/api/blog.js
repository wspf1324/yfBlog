import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/blog/list',
    method: 'post',
    data: query
  })
}

export function getInfo(id) {
  return request({
    url: '/blog/getInfo',
    method: 'post',
    data: { id: id }
  })
}

export function save(data) {
  return request({
    url: '/blog/save',
    method: 'post',
    data: data
  })
}

export function delAll(data) {
  return request({
    url: '/blog/delAll',
    method: 'post',
    data: data
  })
}

export function change(id, field, value) {
  const data = {
    val: id,
    field: field,
    value: value
  }
  return request({
    url: '/blog/change',
    method: 'post',
    data: data
  })
}
