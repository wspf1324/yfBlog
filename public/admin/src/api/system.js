import request from '@/utils/request'

export function clearCache() {
  return request({
    url: '/system/clearCache',
    method: 'post'
  })
}
export function getLineChartData(type) {
  return request({
    url: '/system/getLineChartData',
    method: 'post',
    data: { type: type }
  })
}
export function getLineChartTotal() {
  return request({
    url: '/system/getLineChartTotal',
    method: 'post',
    data: {}
  })
}
export function getPieChartDataTotal() {
  return request({
    url: '/system/getPieChartDataTotal',
    method: 'post',
    data: {}
  })
}
export function getPieChartDataSpider() {
  return request({
    url: '/system/getPieChartDataSpider',
    method: 'post',
    data: {}
  })
}

