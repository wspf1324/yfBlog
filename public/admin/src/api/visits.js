import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/visits/list',
    method: 'post',
    data: query
  })
}

