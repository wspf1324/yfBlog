import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/attachment/list',
    method: 'post',
    data: query
  })
}

export function del(id) {
  return request({
    url: '/attachment/del',
    method: 'post',
    data: { id: id }
  })
}

export function delAll(data) {
  return request({
    url: '/attachment/delAll',
    method: 'post',
    data: data
  })
}
