import { getConfig } from '@/api/setting'

const system = {
  state: {
    basic: null
  },

  mutations: {
    SET_BASIC: (state, data) => {
      state.basic = data
    }
  },

  actions: {
    // 获取基本设置
    GetConfig({ commit }, group) {
      return new Promise((resolve, reject) => {
        getConfig(group).then(response => {
          const data = response.data
          commit('SET_BASIC', data)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default system
