export default {

  // 应用appid
  appid: 'ty9fd2848a039ab554',

  // 应用秘钥
  appSecret: 'ec32286d0718118861afdbf6e401ee81',

  // 配置显示在浏览器标签的title
  title: 'YFBlog后台管理',

  // token在Cookie中存储的天数，默认1天
  cookieExpires: 1,

  // api请求基础路径,注意和代理配合使用
  baseUrl: {
    dev: '',
    pro: 'http://blog.rainfer.cn'
  },

  // 上传路径
  uploadUrl: {
    check: 'http://blog.rainfer.cn/upload/check',
    img: 'http://blog.rainfer.cn/upload/upImage',
    video: '',
    file: 'http://blog.rainfer.cn/upload/upFile'
  },
  // 表单通用设置
  form: {
    global: {
      '*': {
        col: {
          span: 20,
          offset: 2
        }
      },
      upload: {
        props: {
          onSuccess: function(res, file) {
            file.url = res.data.url
          }
        }
      }
    },
    sumbitAndReset: {
      submitBtn: {
        icon: 'el-icon-circle-check',
        col: {
          span: 4,
          offset: 8
        }
      },
      resetBtn: {
        show: true,
        span: 4
      }
    },
    onlySubmit: {
      submitBtn: {
        icon: 'el-icon-circle-check',
        col: {
          span: 4,
          offset: 10
        }
      }
    }
  }
}
