import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss'

Vue.config.productionTip = false
Vue.use(Element)
// 星空特效
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

// 播放器
import APlayer from '@moefe/vue-aplayer'
Vue.use(APlayer)

import { devToolsWarning } from './utils/index'
Vue.use(devToolsWarning)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
