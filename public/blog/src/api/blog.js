import request from '../utils/request'

export function getList(query) {
  return request({
    url: '/getList',
    method: 'post',
    data: query
  })
}

export function getCategories() {
  return request({
    url: '/getCategories',
    method: 'post'
  })
}

export function getCategoryById(id) {
  return request({
    url: '/getCategoryById',
    method: 'post',
    data: { id: id }
  })
}

export function getTagById(id) {
  return request({
    url: '/getTagById',
    method: 'post',
    data: { id: id }
  })
}

export function getTags() {
  return request({
    url: '/getTags',
    method: 'post'
  })
}

export function detail(id) {
  return request({
    url: '/detail',
    method: 'post',
    data: { id: id }
  })
}
