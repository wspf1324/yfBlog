import Vue from 'vue'
import VueRouter from 'vue-router'
import Guide from '../views/Guide.vue'
import About from '../views/About.vue'
import Home from '../views/Home.vue'
import Detail from '../views/Detail.vue'
import Category from '../views/Category.vue'
import Tag from '../views/Tag.vue'
import List from '../views/List.vue'
import NotFound from '../views/NotFound.vue'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
Vue.use(VueRouter)
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to) {
  return VueRouterPush.call(this, to).catch(err => err)
}
NProgress.configure({ showSpinner: true })
const routes = [
  {
    path: '/',
    name: 'guide',
    component: Guide,
    meta: {
      title: "Rainfer's Blog"
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      title: "Home -- Rainfer's Blog"
    }
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/archives',
    name: 'list',
    component: List,
    meta: {
      title: "Archives -- Rainfer's Blog"
    }
  },
  {
    path: '/categories',
    name: 'categories',
    component: Category,
    meta: {
      title: "All Categories -- Rainfer's Blog"
    }
  },
  {
    path: '/category/:id',
    name: 'category',
    component: List
  },
  {
    path: '/tags',
    name: 'tags',
    component: Tag,
    meta: {
      title: "All Tags -- Rainfer's Blog"
    }
  },
  {
    path: '/tag/:tag',
    name: 'tag',
    component: List
  },
  {
    path: '/search/:kwd',
    name: 'search',
    component: List
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    meta: {
      title: "About -- Rainfer's Blog"
    }
  },
  {
    path: '/404',
    name: '404',
    component: NotFound,
    meta: {
      title: "404 Not Found -- Rainfer's Blog"
    }
  },
  {
    path: '*',
    redirect: '/404',
    meta: {
      title: "404 Not Found -- Rainfer's Blog"
    }
  }
]

const router = new VueRouter({
  routes
})
// 路由导航守卫
router.beforeEach((to, from, next) => {
  NProgress.start()
  // 路由发生变化修改页面title
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})
router.afterEach(() => {
  NProgress.done()
})
export default router
