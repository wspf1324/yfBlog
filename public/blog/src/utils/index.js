/**
 * xiegaolei
 */

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  if (!time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (('' + time).length === 10) time = parseInt(time) * 1000
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a|M|T)+}/g, (result, key) => {
    let value
    switch (key) {
      case 'M' :
        value = formatObj['m']
        break
      case 'T' :
        value = formatObj['h']
        break
      default :
        value = formatObj[key]
    }
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (key === 'M') {
      return ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'][value - 1]
    }
    if (key === 'T') {
      return ['深夜', '深夜', '凌晨', '凌晨', '凌晨', '早上', '早上', '早上', '上午', '上午', '上午', '上午', '中午',
        '中午', '下午', '下午', '下午', '下午', '傍晚', '傍晚', '晚上', '晚上', '深夜', '深夜'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

// 当前时间戳
export function getNowTime() {
  return parseInt(Date.now() / 1000)
}

// 格式化时间
export function formatTime(time, option) {
  time = +time * 1000
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
            1 +
            '月' +
            d.getDate() +
            '日' +
            d.getHours() +
            '时' +
            d.getMinutes() +
            '分'
    )
  }
}

// 解析url
export function getQueryObject(url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

export function cleanArray(actual) {
  const newArray = []
  for (let i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i])
    }
  }
  return newArray
}

export function param(json) {
  if (!json) return ''
  return cleanArray(
    Object.keys(json).map(key => {
      if (json[key] === undefined) return ''
      return encodeURIComponent(key) + '=' + encodeURIComponent(json[key])
    })
  ).join('&')
}

export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
        decodeURIComponent(search)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"') +
        '"}'
  )
}

export function html2Text(val) {
  const div = document.createElement('div')
  div.innerHTML = val
  return div.textContent || div.innerText
}

export function objectMerge(target, source) {
  /* Merges two  objects,
       giving the last one precedence */

  if (typeof target !== 'object') {
    target = {}
  }
  if (Array.isArray(source)) {
    return source.slice()
  }
  Object.keys(source).forEach(property => {
    const sourceProperty = source[property]
    if (typeof sourceProperty === 'object') {
      target[property] = objectMerge(target[property], sourceProperty)
    } else {
      target[property] = sourceProperty
    }
  })
  return target
}

export function scrollTo(element, to, duration) {
  if (duration <= 0) return
  const difference = to - element.scrollTop
  const perTick = (difference / duration) * 10
  setTimeout(() => {
    element.scrollTop = element.scrollTop + perTick
    if (element.scrollTop === to) return
    scrollTo(element, to, duration - 10)
  }, 10)
}

export function toggleClass(element, className) {
  if (!element || !className) {
    return
  }
  let classString = element.className
  const nameIndex = classString.indexOf(className)
  if (nameIndex === -1) {
    classString += '' + className
  } else {
    classString =
            classString.substr(0, nameIndex) +
            classString.substr(nameIndex + className.length)
  }
  element.className = classString
}

export const pickerOptions = [
  {
    text: '今天',
    onClick(picker) {
      const end = new Date()
      const start = new Date(new Date().toDateString())
      end.setTime(start.getTime())
      picker.$emit('pick', [start, end])
    }
  },
  {
    text: '最近一周',
    onClick(picker) {
      const end = new Date(new Date().toDateString())
      const start = new Date()
      start.setTime(end.getTime() - 3600 * 1000 * 24 * 7)
      picker.$emit('pick', [start, end])
    }
  },
  {
    text: '最近一个月',
    onClick(picker) {
      const end = new Date(new Date().toDateString())
      const start = new Date()
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
      picker.$emit('pick', [start, end])
    }
  },
  {
    text: '最近三个月',
    onClick(picker) {
      const end = new Date(new Date().toDateString())
      const start = new Date()
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 90)
      picker.$emit('pick', [start, end])
    }
  }
]

export function getTime(type) {
  if (type === 'start') {
    return new Date().getTime() - 3600 * 1000 * 24 * 90
  } else {
    return new Date(new Date().toDateString())
  }
}

export function debounce(func, wait, immediate) {
  let timeout, args, context, timestamp, result

  const later = function() {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔last小于设定时间间隔wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function(...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}

/**
 * This is just a simple version of deep copy
 * Has a lot of edge cases bug
 * If you want to use a perfect deep copy, use lodash's _.cloneDeep
 */
export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'shallowClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

export function uniqueArr(arr) {
  return Array.from(new Set(arr))
}

export function getArrByKey(arr, key) {
  const arrNew = []
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].hasOwnProperty(key)) {
      arrNew.push(arr[i][key])
    }
  }
  return arrNew
}

export function getArrAttrByKey(arr, key, attr, v) {
  let str = ''
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].hasOwnProperty(key) && arr[i].hasOwnProperty(attr) && arr[i][key] === v) {
      str = arr[i][attr]
    }
  }
  return str
}

export function formatImgToArr(imgStr) {
  let imgarr = []
  const avatar = []
  if (imgStr === '') {
    return []
  }
  imgarr = imgStr.split(',')
  for (let i = 0; i < imgarr.length; i++) {
    avatar.push(imgarr[i])
  }
  return avatar
}

export function strRepeat(str, num) {
  return new Array(num + 1).join(str)
}

export function combine(arr) {
  const result = []
  const results = []
  if (arr != null && arr.length > 0) {
    const doExchange = (arr, depth) => {
      for (let i = 0; i < arr[depth].list.length; i++) {
        result[depth] = arr[depth].list[i].name
        if (depth !== arr.length - 1 && arr[depth + 1].list !== undefined && arr[depth + 1].list != null && arr[depth + 1].list.length > 0 && arr[depth + 1].list.name !== '') {
          doExchange(arr, depth + 1)
        } else {
          results.push(result.join(','))
        }
      }
    }
    doExchange(arr, 0)
  }
  return results
}

export function addTime(list, key = 'addTime', hasYear = true, hasMonth = false, hasDay = false) {
  list.forEach(v => {
    let year = 0
    let month = 0
    let day = 0
    let time = 0
    if (v.hasOwnProperty(key)) {
      time = +v[key] * 1000
      const d = new Date(time)
      year = d.getFullYear()
      month = d.getMonth() + 1
      day = d.getDate()
    }
    if (hasYear) {
      v.year = year
    }
    if (hasMonth) {
      v.month = month
    }
    if (hasDay) {
      v.day = day
    }
  })
  return list
}

export function arrGroupBy(datas, keys, callBack) {
  const list = datas || []
  const groups = []
  list.forEach(v => {
    const key = {}
    keys.forEach(k => {
      key[k] = v[k]
    })
    let group = groups.find(v => {
      return v._key === JSON.stringify(key)
    })
    if (!group) {
      group = {
        _key: JSON.stringify(key),
        key: key
      }
      groups.push(group)
    }
    if (callBack) {
      group.data = callBack(group.data, v)
      group.total = group.total || 0
      group.total++
    } else {
      group.data = group.data || []
      group.data.push(v)
    }
  })
  return groups
}

export const devToolsWarning = () => {
  document.addEventListener('DOMContentLoaded', () => {
    if (window.console || 'console' in window) {
      // tslint:disable-next-line:no-console
      console.log(`%c                                                                                                                        
欢888888ba               88                  ad88                            d8'                 88888888ba   88                            
迎      "8b              ""                 d8"                             d8'                  88      "8b  88                            
来      ,8P                                 88                             ""                    88      ,8P  88                            
到aaaaaa8P'  ,adPPYYba,  88  8b,dPPYba,   MM88MMM   ,adPPYba,  8b,dPPYba,         ,adPPYba,      88aaaaaa8P'  88   ,adPPYba,    ,adPPYb,d8  
我""""88'    ""     \`Y8  88  88P'   \`"8a    88     a8P_____88  88P'   "Y8         I8[    ""      88""""""8b,  88  a8"     "8a  a8"    \`Y88  
的    \`8b    ,adPPPPP88  88  88       88    88     8PP"""""""  88                  \`"Y8ba,       88      \`8b  88  8b       d8  8b       88  
博     \`8b   88,    ,88  88  88       88    88     "8b,   ,aa  88                 aa    ]8I      88      a8P  88  "8a,   ,a8"  "8a,   ,d88  
客      \`8b  \`"8bbdP"Y8  88  88       88    88      \`"Ybbd8"'  88                 \`"YbbdP"'      88888888P"   88   \`"YbbdP"'    \`"YbbdP"Y8  
                                                                                                                                aa,    ,88  
                                                                                                                                 "Y8bbdP"    
demo: https://blog.rainfer.cn         author: rainfer520@qq.com         version: v1.00
==========================================================================================================================================
    `, 'color: #dd00dd')
    }
  })
}
