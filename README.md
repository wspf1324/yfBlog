# yfBlog

#### 介绍
基于ThinkPHP5.1.x + Vue 前后端分离框架的博客系统，包含前台和后台。

#### 软件架构
1.  后端:ThinkPHP5.1.x
2.  前端:Vue 2.6.x
3.  UI:element UI

#### 安装教程

1.  git clone
2.  创建数据库，导入sql
3.  域名配置，网站根目录指向`项目根目录/public`
4.  至 `项目根目录/public/admin`下运行`npm install`安装依赖
5.  运行`npm run serve`可以进入开发模式,运行`npm run build`可以打包
6.  打包后拷贝`dist`目录下的文件到`项目根目录/public/manage`(没有此目录就新建一个)
7.  现在可以通过`http://域名/manage/index.html`访问后台(用户名密码均为admin)
8.  进入前台前端代码目录`项目根目录/public/blog`下运行`npm install`安装依赖
9.  运行`npm run serve`可以进入开发模式,运行`npm run build`可以打包
10. 打包后拷贝`dist`目录下的文件到`项目根目录/public`
11. 现在可以通过`http://域名/`访问前台了

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
