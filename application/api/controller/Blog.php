<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\api\controller;

use app\admin\model\blog\Blog as BlogModel;
use app\admin\model\blog\BlogCategory;
use app\admin\model\blog\BlogTag;
use app\admin\model\visit\VisitLog;
use think\facade\Request;
use think\Db;

class Blog extends Base
{
    public function initialize()
    {
        parent::initialize();
        // 统计
        $this->visitLog();
    }

    /**
     * 获取博客列表
     *
     * @throws
     * @route('getList')
     */
    public function getList()
    {
        $cid = input('cid', -1, 'intval');
        $tagId = input('tag', -1, 'intval');
        $title = input('title', '', 'trim');
        $page = input('page', 1, 'intval');
        $lists = BlogModel::getLists($title, $cid, $tagId, -1, 'all', '', '', 'id desc', $page);
        $result['total'] = BlogModel::getTotal($title, $cid, $tagId);
        $result['data'] = $lists;
        ajax_return_ok($result);
    }

    /**
     * 获取博客列表
     *
     * @throws
     * @route('getTags')
     */
    public function getTags()
    {

        $lists = BlogTag::getListsAll();
        ajax_return_ok($lists);
    }

    /**
     * 获取分类列表
     *
     * @throws
     * @route('getCategories')
     */
    public function getCategories()
    {

        $lists = BlogModel::getAllCategoriesCount();
        ajax_return_ok($lists);
    }

    /**
     * 获取分类
     *
     * @throws
     * @route('getCategoryById')
     */
    public function getCategoryById()
    {
        $id = input('id', 0, 'intval');
        if (!$id) {
            ajax_return_error('分类id不存在');
        }
        $data = BlogCategory::get($id);
        ajax_return_ok($data);
    }

    /**
     * 获取标签
     *
     * @throws
     * @route('getTagById')
     */
    public function getTagById()
    {
        $id = input('id', 0, 'intval');
        if (!$id) {
            ajax_return_error('标签id不存在');
        }
        $data = BlogTag::get($id);
        ajax_return_ok($data);
    }

    /**
     * 获取标签
     *
     * @throws
     * @route('detail')
     */
    public function detail()
    {
        $id = input('id', 0, 'intval');
        if (!$id) {
            ajax_return_error('博客id不存在');
        }
        $data = BlogModel::get($id);
        // 累计文章访问量
        BlogModel::where('id', $id)->setInc('visit');
        ajax_return_ok($data);
    }

    private function visitLog()
    {
        // is cli ?
        if ($this->request->isCli()) {
            return;
        }
        // ip is null ?
        $ip = $this->request->ip(1);
        if (!$ip) {
            return;
        }
        // ua
        $ua = $this->request->server('HTTP_USER_AGENT');
        // main spider
        $visitor = ua_is_spider($ua);
        if (!$visitor && ua_is_bot($ua)) {
            // other bot
            return;
        }
        // human
        $visitor = $visitor ?: 'human';
        $arr = array('human', 'google', 'bing', 'baidu', 'sogou', 'yisou');
        if (!in_array($visitor, $arr)) {
            return;
        }
        // visit_count
        $date = date('Y-m-d');
        $sql = sprintf("INSERT INTO %s (`countDate`, `total`, `%s`) VALUES ('%s',1,1) ON DUPLICATE KEY UPDATE `%s` = `%s` + 1,`total`=`total`+1;", (config('database.prefix') . 'visit_count'), $visitor, $date, $visitor, $visitor);
        Db::execute($sql);
        // os
        $os = ua_has_os($ua);
        // browser
        $browser = ua_has_browser($ua);
        // referer
        $referer = $this->request->server("HTTP_REFERER");
        $referer = has_referer($referer);
        $referer = $referer ?: '';
        $data = [
            'visitor' => $visitor,
            'ip' => $ip,
            'uri' => $this->request->server("REQUEST_URI"),
            'os' => $os,
            'browser' => $browser,
            'from' => $referer,
        ];
        VisitLog::create($data);
    }
}
