<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\api\controller;

use app\common\controller\Api;

/**
 * api基类 -- 不验证用户登录
 * @author rainfer520@qq.com
 *
 */
class Base extends Api
{
    public function initialize()
    {
        parent::initialize();
    }
}
