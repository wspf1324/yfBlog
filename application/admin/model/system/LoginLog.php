<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\system;

/**
 * 管理员登录日志
 */
class LoginLog extends \think\Model
{
    /**
     * 获取管理员登录日志列表
     *
     * @param $uid
     * @param $userName
     * @param $loginIp
     * @param $startTime
     * @param $endTime
     * @param $myOrder
     * @param $page
     * @param $pSize
     *
     * @return array
     *
     * @throws
     */
    public static function getLists($uid, $userName, $loginIp, $startTime, $endTime, $myOrder, $page, $pSize)
    {
        $where = [];
        if ($uid) {
            $where[] = ['uid', '=', $uid];;
        }
        if ($userName) {
            $where[] = ['userName', 'like', '%' . $userName . '%'];
        }
        if ($loginIp) {
            $where[] = ['loginIp', '=', $loginIp];
        }
        if ($startTime) {
            $where[] = ['loginTime', '>=', $startTime];
        }
        if ($endTime) {
            $where[] = ['loginTime', '<=', $endTime];
        }
        return self::where($where)->order($myOrder)->page($page, $pSize)->select();
    }

    /**
     * 查询管理员登录日志数量
     *
     * @param $uid
     * @param $userName
     * @param $loginIp
     * @param $startTime
     * @param $endTime
     *
     * @return int
     *
     * @throws
     */
    public static function getTotal($uid, $userName, $loginIp, $startTime, $endTime)
    {
        $where = [];
        if ($uid) {
            $where[] = ['uid', '=', $uid];;
        }
        if ($userName) {
            $where[] = ['userName', 'like', '%' . $userName . '%'];
        }
        if ($loginIp) {
            $where[] = ['loginIp', '=', $loginIp];
        }
        if ($startTime) {
            $where[] = ['loginTime', '>=', $startTime];
        }
        if ($endTime) {
            $where[] = ['loginTime', '<=', $endTime];
        }
        return self::where($where)->count();
    }
}
