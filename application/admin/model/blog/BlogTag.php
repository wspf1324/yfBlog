<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\blog;

class BlogTag extends \think\Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'addTime';

    /**
     * 获取标签列表
     *
     * @param $title
     * @param $isDel
     * @param $status
     * @param $order
     * @param $page
     * @param $psize
     *
     * @return array
     * @throws
     */
    public static function getLists($title = '', $isDel = -1, $status = -1, $order = 'id desc', $page = 1, $psize = 10)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($isDel !== -1) {
            $where[] = ['isDel', '=', $isDel];
        }
        if ($status !== -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->order($order)->page($page, $psize)->select();
    }

    /**
     * 获取标签数量
     *
     * @param $title
     * @param $isDel
     * @param $status
     *
     * @return int
     * @throws
     */
    public static function getTotal($title = '', $isDel = -1, $status = -1)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($isDel !== -1) {
            $where[] = ['isDel', '=', $isDel];
        }
        if ($status !== -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->count();
    }

    /**
     * 根据状态获取标签数
     *
     * @param $status
     *
     * @return int
     */
    public static function getCountByStatus($status = -1)
    {
        $where = [];
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        $where[] = ['isDel', '=', 0];
        $count = self::where($where)->count();
        return $count;
    }

    /**
     * 获取全部标签id数组
     *
     * @param $status
     *
     * @return array
     */
    public static function getTagIds()
    {
        $ids = self::column('id');
        return $ids;
    }

    /**
     * 获取标签列表--不分页
     *
     * @param $title
     * @param $pid
     * @param $isDel
     *
     * @return array
     *
     * @throws
     */
    public static function getListsAll($title = '', $isDel = -1)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }

        if ($isDel !== -1) {
            $where[] = ['isDel', '=', $isDel];
        }
        return self::field('id,title')->where($where)->order('sort,id')->select()->toArray();
    }

    /**
     * 删除标签(永久删除)
     *
     * @param $ids
     *
     * @return boolean
     * @throws
     */
    public static function del($ids)
    {
        $ids = is_array($ids) ? $ids : explode(',', $ids);
        self::StartTrans();
        try {
            // 删除主表
            self::destroy($ids);
            // 删除文章标签关联表
            BlogTagAssoc::where('tagId', 'in', $ids)->delete();
            self::commit();
        } catch (\think\Exception $e) {
            self::rollback();
            return false;
        }
        return true;
    }
}
