<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\blog;

class BlogComments extends \think\Model
{
    /**
     * 根据状态获取评论数
     *
     * @param $status
     *
     * @return int
     */
    public static function getCountByStatus($status = -1)
    {
        $where = [];
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        $where[] = ['isDel', '=', 0];
        $count = self::where($where)->count();
        return $count;
    }
}
