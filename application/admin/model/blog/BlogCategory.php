<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\blog;

class BlogCategory extends \think\Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'addTime';

    /**
     * 根据状态获取分类数
     *
     * @param $status
     *
     * @return int
     */
    public static function getCountByStatus($status = -1)
    {
        $where = [];
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        $where[] = ['isDel', '=', 0];
        $count = self::where($where)->count();
        return $count;
    }

    /**
     * 获取文章分类列表--不分页
     *
     * @param $title
     * @param $pid
     * @param $isDel
     *
     * @return array
     *
     * @throws
     */
    public static function getListsAll($title = '', $pid = -1, $isDel = -1)
    {
        $where = [];
        if ($title) {
            $where[] = ['a.title', 'like', '%' . $title . '%'];
        }
        if ($pid !== -1) {
            $where[] = ['a.pid', '=', $pid];
        }

        if ($isDel !== -1) {
            $where[] = ['a.isDel', '=', $isDel];
        }
        return self::alias('a')->field('a.*,b.title as parentName')
            ->leftJoin('blog_category b', 'a.pid=b.id')
            ->where($where)->order('sort,id')->select()->toArray();
    }

    /**
     * 获取含自身id及子孙id的分类
     *
     * @param $pid
     *
     * @return array
     *
     * @throws
     */
    public static function getChildrenIds($pid)
    {
        $list = self::getListsAll();
        $ids = \TreeUtil::getChilds($list, $pid, 'id', 'pid', true, true);
        return $ids;
    }

    /**
     * 删除分类(永久删除)
     *
     * @param $ids
     *
     * @return boolean
     * @throws
     */
    public static function del($ids)
    {
        $ids = is_array($ids) ? $ids : explode(',', $ids);
        $delIds = self::getIds($ids);
        // 获取这些分类下文章ids
        $delBlogIds = Blog::where('cid', 'in', $delIds)->column('cid');
        self::StartTrans();
        try {
            // 删除分类
            self::destroy($delIds);
            // 删除该分类文章
            Blog::del($delBlogIds);
            // TODO 删除附件数据 删除评论
            self::commit();
        } catch (\think\Exception $e) {
            self::rollback();
            return false;
        }
        return true;
    }

    private static function getIds($ids)
    {
        $ids = is_array($ids) ? $ids : explode(" ", $ids);
        $result = [];
        $list = self::getListsAll();
        while ($ids) {
            $pid = array_shift($ids);
            $_ids = \TreeUtil::getChilds($list, $pid, 'id', 'pid', true, true);
            $result = array_merge($result, $_ids);
            $ids = array_diff($ids, $_ids);
        }
        return $result;
    }
}
