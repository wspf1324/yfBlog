<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\setting;

class Attachment extends \think\Model
{
    protected $name = 'system_attachment';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'createTime';

    /**
     * 获取附件列表
     *
     * @param $name
     * @param $attType
     * @param $startTime
     * @param $endTime
     * @param $page
     * @param $pSize
     *
     * @return array
     *
     * @throws
     */
    public static function getLists($name, $attType, $startTime, $endTime, $page, $pSize)
    {
        $where = [];
        if ($name) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
        if ($attType) {
            $where[] = ['attType', '=', $attType];
        }
        if ($startTime) {
            $where[] = ['createTime', '>=', $startTime];
        }
        if ($endTime) {
            $where[] = ['createTime', '<=', $endTime];
        }
        return self::where($where)->order('id desc')->page($page, $pSize)->select();
    }

    /**
     * 查询附件数量
     *
     * @param $name
     * @param $attType
     * @param $startTime
     * @param $endTime
     *
     * @return int
     *
     * @throws
     */
    public static function getTotal($name, $attType, $startTime, $endTime)
    {
        $where = [];
        if ($name) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
        if ($attType) {
            $where[] = ['attType', '=', $attType];
        }
        if ($startTime) {
            $where[] = ['createTime', '>=', $startTime];
        }
        if ($endTime) {
            $where[] = ['createTime', '<=', $endTime];
        }
        return self::where($where)->count();
    }

    /**
     * 检测是否存在附件,存在返回url,否则返回false
     *
     * @throws
     */
    public static function check($md5)
    {
        $file = self::where('md5', $md5)->find();
        if (empty($file)) {
            return false;
        } else {
            return ['url' => $file['attDir'], 'name' => $file['name']];
        }
    }
}
