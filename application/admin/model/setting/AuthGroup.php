<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\setting;

/**
 * 用户组相关操作
 */
class AuthGroup extends \think\Model
{
    protected $autoWriteTimestamp = true;
    protected $updateTime = 'updateTime';


    /**
     * 获取分组列表
     *
     * @param $title
     * @param $status
     * @param $myOrder
     * @param $page
     * @param $pSize
     *
     * @return array
     *
     * @throws
     */
    public static function getLists($title, $status, $myOrder, $page, $pSize)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->order($myOrder)->page($page, $pSize)->select();
    }

    /**
     * 获取所有的列表,不分页
     *
     * @param $status
     * @param $myOrder
     *
     * @return array
     *
     * @throws
     */
    public static function getListsAll($status, $myOrder = 'id Asc')
    {
        $where = [];
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->order($myOrder)->select();
    }

    /**
     * 获取分组数量
     *
     * @param $title
     * @param $status
     *
     * @return int
     *
     * @throws
     */
    public static function getTotal($title, $status)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->count();
    }

}
