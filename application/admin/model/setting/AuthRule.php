<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\setting;

use app\admin\model\setting\AuthGroup as AuthGroupModel;
use TreeUtil;

/**
 * 用户组权限相关操作
 */
class AuthRule extends \think\Model
{
    /**
     * 通过ID获取权限信息
     *
     * @param $ruleId
     *
     * @return array
     *
     * @throws
     */
    public static function getRuleById($ruleId)
    {
        $where = ['id', '=', $ruleId];
        return self::where($where)->find();
    }

    /**
     * 通过PID获取权限信息
     *
     * @param $pid
     *
     * @return array
     *
     * @throws
     */
    public static function getRuleByPid($pid)
    {
        $where = ['pid', '=', $pid];
        return self::where($where)->find();
    }

    /**
     * 权限id 数组
     *
     * @param $ruleIds
     *
     * @return array
     *
     * @throws
     */
    public static function getRuleByIds($ruleIds)
    {
        $where = [
            ['status', '=', 1]
        ];
        if ($ruleIds !== null) {
            $where[] = ['id', 'in', $ruleIds];
        }
        return self::where($where)->order('sorts')->select();
    }

    /**
     * 根据名称获取权限
     *
     * @param $name
     *
     * @return array
     *
     * @throws
     */
    public static function getRuleByName($name)
    {
        return self::where(['name' => $name])->find();
    }

    /**
     * 获取列表 不分页
     *
     * @param $title
     * @param $status
     * @param $myOrder
     *
     * @return array
     *
     * @throws
     */
    public static function getLists($title, $status, $myOrder)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->order($myOrder)->select();
    }

    /**
     * 获取所有的列表,不分页
     *
     * @param $status
     * @param $myOrder
     *
     * @return array
     *
     * @throws
     */
    public static function getListsAll($status, $myOrder = 'id')
    {
        $where = [];
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->order($myOrder)->select();
    }

    /**
     * 获取数量
     *
     * @param $title
     * @param $status
     *
     * @return int
     *
     * @throws
     */
    public static function getTotal($title, $status)
    {
        $where = [];
        if ($title) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        if ($status != -1) {
            $where[] = ['status', '=', $status];
        }
        return self::where($where)->count();
    }

    /**
     * 获取权限组权限 格式化成树状
     *
     * @param $groupId int 分组id
     *
     * @return array
     */
    public static function getAuthByGroupIdToTree($groupId)
    {
        if ($groupId === 1) {
            $ruleIds = null;
        } else {
            $group = AuthGroupModel::find($groupId);
            if ($group['status'] != 1 || empty($group['rules'])) {
                return [];
            }
            $ruleIds = explode(',', $group['rules']);
        }
        $rules = self::getRuleByIds($ruleIds);
        $rules = TreeUtil::listToTreeMulti($rules, 0, 'id', 'pid', 'children');
        return $rules;
    }

    /**
     * 转换成路由数组
     *
     * @param $rules
     *
     * @return array
     */
    public static function getRouters($rules)
    {
        $router = [];
        foreach ($rules as &$data) {
            $temp = [];
            $temp['path'] = $data['path'];
            $temp['component'] = $data['component'];
            $temp['name'] = $data['name'];
            if ($data['hidden'] > -1) {
                $temp['hidden'] = (boolean)$data['hidden'];
            }
            if ($data['alwaysShow'] > -1) {
                $temp['alwaysShow'] = (boolean)$data['alwaysShow'];
            }
            if ($data['redirect']) {
                $temp['redirect'] = $data['redirect'];
            }
            $temp['meta']['title'] = $data['title'];
            $temp['meta']['icon'] = $data['icon'];
            if ($data['noCache'] > -1) {
                $temp['meta']['noCache'] = (boolean)$data['noCache'];
            }
            // $routers[] = $temp;
            if (!empty($data['children'])) {
                $temp['children'] = self::getRouters($data['children']);
            }
            $router[] = $temp;
        }
        return $router;
    }

    /**
     * 根据名称获取权限
     *
     * @param $name
     * @param $groupId
     *
     * @return bool
     */
    public static function hasAccessByName($name, $groupId)
    {
        $rule = self::getRuleByName($name);
        // 未匹配到,默认有权限
        if (empty($rule)) {
            return true;
        }
        if ($rule['status'] === 0) {
            return false;
        }
        $group = AuthGroupModel::find($groupId);
        if ($group['status'] != 1 || empty($group['rules'])) {
            return false;
        }
        $myRuleIds = explode(',', $group['rules']);
        if (in_array($rule['id'], $myRuleIds)) {
            return true;
        } else {
            return false;
        }
    }
}
