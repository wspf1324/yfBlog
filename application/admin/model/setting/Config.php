<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\setting;

class Config extends \think\Model
{

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'system_config';


    /**
     * 获取配置分类
     *
     * @param $id
     *
     * @return array
     * @throws
     */
    public static function getAll($group = 'basic')
    {
        $where = [];
        if ($group) {
            $where[] = ['group', '=', $group];
        }
        return self::where($where)->column('value', 'name');
    }
}
