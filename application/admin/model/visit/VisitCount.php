<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\model\visit;

class VisitCount extends \think\Model
{

    /**
     * 获取统计列表
     *
     * @param $countDate
     * @param $startTime
     * @param $endTime
     * @param $order
     * @param $page
     * @param $psize
     *
     * @return array
     * @throws
     */
    public static function getLists($countDate = 'all', $startTime = '', $endTime = '', $order = 'id desc', $page = 1, $psize = 10)
    {
        // 创建时间
        $whereTime = [];
        if ($countDate == 'custom') {
            $whereTime[] = ['countDate', 'between', [$startTime, $endTime]];
        } else {
            $whereTime = where_between_time($countDate, 'countDate', 'Y-m-d');
        }
        return self::where($whereTime)->order($order)->page($page, $psize)->select();
    }

    /**
     * 获取统计数量
     *
     * @param $countDate
     * @param $startTime
     * @param $endTime
     *
     * @return int
     * @throws
     */
    public static function getTotal($countDate = 'all', $startTime = '', $endTime = '')
    {
        // 创建时间
        $whereTime = [];
        if ($countDate == 'custom') {
            $whereTime[] = ['countDate', 'between', [$startTime, $endTime]];
        } else {
            $whereTime = where_between_time($countDate, 'countDate');
        }
        return self::where($whereTime)->count();
    }


    /**
     * 统计起止日期数
     *
     * @param $start string 起始日期 'Y-M-D'
     * @param $end string 截止日期
     *
     * @return array
     *
     * @throws
     */
    public static function getTotalByDays($start, $end)
    {
        $list = self::field("countDate,total")
            ->whereBetweenTime('countDate', $start, $end)
            ->select()
            ->toArray();
        $list = array_column($list, 'total', 'countDate');
        $dates = date_rang($start, $end);
        foreach ($dates as &$date) {
            if (array_key_exists($date, $list)) {
                $date = [
                    'date' => $date,
                    'count' => $list[$date]
                ];
            } else {
                $date = [
                    'date' => $date,
                    'count' => 0
                ];
            }
        }
        return $dates;
    }

    /**
     * 获取今日数据
     *
     * @return array
     *
     * @throws
     */
    public static function getToday()
    {
        $list = self::whereTime('countDate', 'today')->find();
        return $list;
    }
}
