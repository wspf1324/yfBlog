<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\blog;

use app\admin\controller\Base;
use app\admin\model\blog\BlogTag as BlogTagModel;

class Tag extends Base
{

    /**
     * 获取标签列表
     *
     * @route('tag/list')
     */
    public function index()
    {
        $title = input('title', '', 'trim');
        $isDel = input('isDel', -1, 'intval');
        $status = input('status', -1, 'intval');
        $sort = input('sort', '', 'trim');
        $page = input('page', 1, 'intval');
        $psize = input('psize', 10, 'intval');
        $lists = BlogTagModel::getLists($title, $isDel, $status, $sort, $page, $psize);
        $result['total'] = BlogTagModel::getTotal($title, $isDel, $status);
        $result['data'] = $lists;
        ajax_return_ok($result);
    }

    /**
     * 列表 - 不分页
     *
     * @route('tag/getAll')
     */
    public function getAll()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $title = input('title', '', 'trim');
            $isDel = input('isDel', -1, 'intval');

            $lists = BlogTagModel::getListsAll($title, $isDel);
            ajax_return_ok($lists);
        }
    }

    /**
     * 修改
     *
     * @throws
     * @route('tag/change')
     */
    public function change()
    {

        $val = input('val', '', 'int');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($field)) {
            ajax_return_error('参数有误！');
        }
        $res = BlogTagModel::where('id', $val)->update([$field => $value]);
        if ($res !== false) {
            ajax_return_ok([], '修改成功！');
        } else {
            ajax_return_error('修改失败！');
        }
    }

    /**
     * 批量删除
     *
     * @throws
     * @route('tag/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            if (BlogTagModel::del($ids)) {
                ajax_return_ok([], '删除成功');
            } else {
                ajax_return_error('删除失败');
            }
        }
    }

    /**
     * 获取标签信息
     *
     * @throws
     * @route('tag/getInfo')
     */
    public function getInfo()
    {
        $id = input('id', 0, 'intval');
        $info = BlogTagModel::get($id);
        ajax_return_ok($info);
    }

    /**
     * 增加/修改
     *
     * @throws
     * @route('tag/save')
     */
    public function save()
    {
        $id = input('id', 0, 'intval');
        $data = [
            'title' => input('title', '', 'trim'),
            'sort' => input('sort', 0, 'intval'),
            'status' => input('status', 0, 'intval'),
            'isDel' => input('isDel', 0, 'intval')
        ];
        if ($id) {
            $data['id'] = $id;
            // 更新
            BlogTagModel::StartTrans();
            try {
                // 更新标签
                BlogTagModel::update($data);
                BlogTagModel::commit();
            } catch (\think\Exception $e) {
                BlogTagModel::rollback();
                ajax_return_error($e->getMessage());
            }
            ajax_return_ok($data, '修改成功！');
        } else {
            BlogTagModel::StartTrans();
            try {
                // 增加标签
                $tag = BlogTagModel::create($data);
                BlogTagModel::commit();
            } catch (\think\Exception $e) {
                BlogTagModel::rollback();
                ajax_return_error($e->getMessage());
            }
            ajax_return_ok($tag, '增加成功！');
        }
    }
}
