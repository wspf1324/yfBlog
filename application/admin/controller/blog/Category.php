<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\blog;

use app\admin\controller\Base;
use app\admin\model\blog\BlogCategory as BlogCategoryModel;

class Category extends Base
{
    /**
     * 列表 - 不分页
     *
     * @route('category/getAll')
     */
    public function getAll()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $title = input('title', '', 'trim');
            $pid = input('pid', -1, 'intval');
            $isDel = input('isDel', -1, 'intval');

            $lists = BlogCategoryModel::getListsAll($title, $pid, $isDel);
            ajax_return_ok($lists);
        }
    }

    /**
     * 修改
     *
     * @throws
     * @route('category/change')
     */
    public function change()
    {

        $val = input('val', '', 'int');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($field)) {
            ajax_return_error('参数有误！');
        }
        $res = BlogCategoryModel::where('id', $val)->update([$field => $value]);
        if ($res !== false) {
            ajax_return_ok([], '修改成功！');
        } else {
            ajax_return_error('修改失败！');
        }
    }

    /**
     * 批量删除
     *
     * @throws
     * @route('category/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            if (BlogCategoryModel::del($ids)) {
                ajax_return_ok([], '删除成功');
            } else {
                ajax_return_error('删除失败');
            }
        }
    }

    /**
     * 获取分类信息
     *
     * @throws
     * @route('category/getInfo')
     */
    public function getInfo()
    {
        $id = input('id', 0, 'intval');
        $info = BlogCategoryModel::get($id);
        ajax_return_ok($info);
    }

    /**
     * 增加/修改
     *
     * @throws
     * @route('category/save')
     */
    public function save()
    {
        $id = input('id', 0, 'intval');
        $data = [
            'pid' => input('pid', 0, 'intval'),
            'title' => input('title', '', 'trim'),
            'intr' => input('intr', '', 'trim'),
            'image' => input('image', '', 'trim'),
            'status' => input('status', 0, 'intval'),
            'sort' => input('sort', 0, 'intval'),
            'isDel' => input('isDel', 0, 'intval'),
            'addTime' => input('addTime', time(), 'intval'),
            'hidden' => input('hidden', 0, 'intval')
        ];
        if ($id) {
            $data['id'] = $id;
            // 更新分类
            if (BlogCategoryModel::update($data)) {
                ajax_return_ok($data, '修改成功！');
            } else {
                ajax_return_error('修改失败！');
            }
        } else {
            if (BlogCategoryModel::create($data)) {
                ajax_return_ok($data, '增加成功！');
            } else {
                ajax_return_error('增加失败！');
            }
        }
    }
}
