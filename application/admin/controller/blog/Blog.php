<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\blog;

use app\admin\controller\Base;
use app\admin\model\blog\Blog as BlogModel;
use app\admin\model\blog\BlogContent as BlogContentModel;

class Blog extends Base
{
    /**
     * 获取文章列表
     *
     * @route('blog/list')
     */
    public function index()
    {
        $title = input('title', '', 'trim');
        $cid = input('cid', -1, 'intval');
        $isDel = input('isDel', -1, 'intval');
        $createTime = input('createTime', 'all', 'trim');
        $startTime = input('startTime', '', 'strtotime');
        $endTime = input('endTime', '', 'strtotime');
        $sort = input('sort', '', 'trim');
        $page = input('page', 1, 'intval');
        $psize = input('psize', 10, 'intval');
        $lists = BlogModel::getLists($title, $cid, -1, $isDel, $createTime, $startTime, $endTime, $sort, $page, $psize);
        $result['total'] = BlogModel::getTotal($title, $cid, -1, $isDel, $createTime, $startTime, $endTime);
        $result['data'] = $lists;
        ajax_return_ok($result);
    }

    /**
     * 修改
     *
     * @throws
     * @route('blog/change')
     */
    public function change()
    {

        $val = input('val', '', 'int');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($field)) {
            ajax_return_error('参数有误！');
        }
        $res = BlogModel::where('id', $val)->update([$field => $value]);
        if ($res !== false) {
            ajax_return_ok([], '修改成功！');
        } else {
            ajax_return_error('修改失败！');
        }
    }

    /**
     * 批量删除
     *
     * @throws
     * @route('blog/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            if (BlogModel::del($ids)) {
                ajax_return_ok([], '删除成功');
            } else {
                ajax_return_error('删除失败');
            }
        }
    }

    /**
     * 获取文章信息
     *
     * @throws
     * @route('blog/getInfo')
     */
    public function getInfo()
    {
        $id = input('id', 0, 'intval');
        $info = BlogModel::get($id);
        ajax_return_ok($info);
    }

    /**
     * 增加/修改
     *
     * @throws
     * @route('blog/save')
     */
    public function save()
    {
        $id = input('id', 0, 'intval');
        $tags = input('tags/a', [], 'trim');
        $content = input('content', '');
        $data = [
            'cid' => input('cid', 1, 'intval'),
            'title' => input('title', '', 'trim'),
            'stitle' => input('stitle', '', 'trim'),
            'keyword' => input('keyword', '', 'trim'),
            'author' => $this->user['realName'] ?: $this->user['userName'],
            'adminId' => $this->user['id'],
            'synopsis' => input('synopsis', '', 'trim'),
            'image' => input('image', '', 'trim'),
            'sort' => input('sort', 0, 'intval'),
            'url' => input('url', '', 'trim'),
            'source' => input('source', '', 'trim'),
            'addTime' => input('addTime', time(), 'intval'),
            'displayTime' => input('displayTime', time(), 'intval'),
            'status' => input('status', 0, 'intval'),
            'isDel' => input('isDel', 0, 'intval'),
            'isHot' => input('isHot', 0, 'intval'),
            'isBanner' => input('isBanner', 0, 'intval')
        ];
        if ($id) {
            $data['id'] = $id;
            // 更新
            BlogModel::StartTrans();
            try {
                // 更新文章
                BlogModel::update($data);
                // 更新文章标签
                BlogModel::updateTags($id, $tags);
                // 更新文章内容
                BlogContentModel::where('nid', $id)->update(['content' => $content]);
                BlogModel::commit();
            } catch (\think\Exception $e) {
                BlogModel::rollback();
                ajax_return_error($e->getMessage());
            }
            ajax_return_ok($data, '修改成功！');
        } else {
            BlogModel::StartTrans();
            try {
                // 增加文章
                $blog = BlogModel::create($data);
                $id = $blog->id;
                // 更新文章标签
                BlogModel::updateTags($id, $tags);
                // 文章内容
                BlogContentModel::create(['nid' => $id, 'content' => $content]);
                BlogModel::commit();
            } catch (\think\Exception $e) {
                BlogModel::rollback();
                ajax_return_error($e->getMessage());
            }
            ajax_return_ok($blog, '增加成功！');
        }
    }
}
