<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\admin\model\blog\Blog;
use app\admin\model\blog\BlogCategory;
use app\admin\model\blog\BlogComments;
use app\admin\model\blog\BlogTag;
use app\admin\model\visit\VisitCount;
use think\facade\Cache;

/**
 * 系统首页
 * @author rainfer520@qq.com
 */
class Index extends Base
{

    /**
     * 清理缓存
     *
     * @throws
     * @route('system/clearCache')
     */
    public function clearCache()
    {
        Cache::clear();
        ajax_return_ok([], '清理成功！');
    }

    /**
     * 获取折线图表数据
     *
     * @throws
     * @route('system/getLineChartData')
     */
    public function getLineChartData()
    {
        //$data = Cache::get('lineChartData');
        $data = [];
        if (!$data) {
            $end = date('Y-m-d', time());
            $start = date('Y-m-d', strtotime('-6 days'));
            $dates = date_rang($start, $end, 'm-d');
            $visitList = VisitCount::getTotalByDays($start, $end);
            $visitList = array_column($visitList, 'count');
            $data = [
                'expectedData' => ['list' => $visitList, 'title' => '访问统计'],
                'xAxis' => $dates
            ];
            Cache::set('lineChartData', $data, 1800);
        }
        ajax_return_ok($data, '成功！');
    }

    /**
     * 获取统计数据
     *
     * @throws
     * @route('system/getLineChartTotal')
     */
    public function getLineChartTotal()
    {
        // 文章总数
        $blog = Blog::getCountByStatus(1);
        // 分类总数
        $blogCategory = BlogCategory::getCountByStatus(1);
        // 标签总数
        $blogTag = BlogTag::getCountByStatus(1);
        // 评论总数
        $blogComments = BlogComments::getCountByStatus(1);
        $data = [
            ['total' => $blog, 'color' => '#36a3f7', 'iconClass' => 'blog', 'title' => '文章总数', 'url' => 'blog/blog'],
            ['total' => $blogCategory, 'color' => '#324157', 'iconClass' => 'fenlei', 'title' => '分类总数', 'url' => 'b'],
            ['total' => $blogTag, 'color' => '#34bfa3', 'iconClass' => 'biaoqian', 'title' => '标签总数', 'url' => 'c'],
            ['total' => $blogComments, 'color' => '#cf9236', 'iconClass' => 'pinglun', 'title' => '评论总数', 'url' => 'd']
        ];
        ajax_return_ok($data, '成功！');
    }

    /**
     * 获取分类文章饼图表数据 按分类
     *
     * @throws
     * @route('system/getPieChartDataTotal')
     */
    public function getPieChartDataTotal()
    {
        $categorys = BlogCategory::getListsAll('', 0, -1);
        $list = $xAxis = [];
        foreach ($categorys as $item) {
            $list[] = [
                'name' => $item['title'],
                'value' => Blog::getCountByCids($item['id'])
            ];
            $xAxis[] = $item['title'];
        }
        $data = [
            'expectedData' => ['list' => $list, 'title' => '类别文章数统计'],
            'xAxis' => $xAxis
        ];
        ajax_return_ok($data, '成功！');
    }

    /**
     * 获取爬虫饼图表数据
     *
     * @throws
     * @route('system/getPieChartDataSpider')
     */
    public function getPieChartDataSpider()
    {
        $today = VisitCount::getToday();
        $types = ['human', 'google', 'bing', 'baidu', 'sogou', 'yisou'];
        $list = [];
        foreach ($types as $type) {
            $list[] = [
                'name' => $type,
                'value' => empty($today[$type]) ? 0 : $today[$type]
            ];
        }
        $data = [
            'expectedData' => ['list' => $list, 'title' => '今日爬虫访问统计'],
            'xAxis' => $types
        ];
        ajax_return_ok($data, '成功！');
    }
}
