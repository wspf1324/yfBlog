<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\admin\model\setting\Admin as AdminModel;
use think\captcha\Captcha;
use think\Controller;

/**
 * 后台登陆
 * @author rainfer520@qq.com
 */
class Login extends Controller
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 登录
     *
     * @route('login/loginIn')
     */
    public function index()
    {
        if ($this->request->isPost()) {
            //接收数据
            $data = [
                'userName' => input('userName', '', 'trim'),
                'password' => input('password', '', 'trim'),
                'verify' => input('verify', '', 'trim')
            ];
            $validate = validate('setting.Admin');
            $result = $validate->scene('login')->check($data);
            if (!$result) {
                $error = $validate->getError();
                ajax_return_error($error);
            }
            // 登录验证并获取包含访问令牌的用户
            $result = AdminModel::login($data['userName'], $data['password']);
            ajax_return_ok($result, '登录成功');
        }
    }

    /**
     * 验证码
     *
     * @param string $id
     *
     * @return mixed
     * @route('login/verify')
     */
    public function verify()
    {
        ob_end_clean();
        $captcha = new Captcha(config('yfcmf.verify'));
        return $captcha->entry();
    }
}
