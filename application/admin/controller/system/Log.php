<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\system;

use app\admin\model\system\LoginLog;
use app\admin\controller\Base;

/**
 * 管理员登录日志管理
 * @author rainfer520@qq.com
 */
class Log extends Base
{
    /**
     * 列表
     *
     * @route('log/list')
     */
    public function index()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $userName = input('userName', '', 'trim');
            $uid = input('uid', '', 'trim');
            $loginIp = input('loginIp', '', 'trim');
            $startTime = input('startTime', '', 'strtotime');
            $endTime = input('endTime', '', 'strtotime');
            $order = input('order/a', 'id desc');
            $page = input('page', 1, 'intval');
            $psize = input('psize', 10, 'intval');

            $lists = LoginLog::getLists($uid, $userName, $loginIp, $startTime, $endTime, $order, $page, $psize);
            $result['total'] = LoginLog::getTotal($uid, $userName, $loginIp, $startTime, $endTime);
            $result['data'] = $lists;
            ajax_return_ok($result);
        }
    }

}
