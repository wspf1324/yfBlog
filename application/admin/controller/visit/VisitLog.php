<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\visit;

use app\admin\controller\Base;
use app\admin\model\visit\VisitLog as VisitLogModel;
use think\Db;

class VisitLog extends Base
{

    /**
     * 获取标签列表
     *
     * @route('visit/list')
     */
    public function index()
    {
        $visitor = input('visitor', '-1', 'trim');
        $ip = input('ip', '', 'trim');
        $uri = input('uri', '', 'trim');
        $visitTime = input('visitTime', 'all', 'trim');
        $startTime = input('startTime', '', 'strtotime');
        $endTime = input('endTime', '', 'strtotime');
        $sort = input('sort', '', 'trim');
        $page = input('page', 1, 'intval');
        $psize = input('psize', 10, 'intval');
        $lists = VisitLogModel::getLists($visitor, $ip, $uri, $visitTime, $startTime, $endTime, $sort, $page, $psize);
        $result['total'] = VisitLogModel::getTotal($visitor, $ip, $uri, $visitTime, $startTime, $endTime);
        $result['data'] = $lists;
        ajax_return_ok($result);
    }

    /**
     * 批量删除
     *
     * @throws
     * @route('visit/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            if (VisitLogModel::destroy($ids)) {
                ajax_return_ok([], '删除成功');
            } else {
                ajax_return_error('删除失败');
            }
        }
    }

    /**
     * 清空
     *
     * @throws
     * @route('visit/clear')
     */
    public function clear()
    {
        $result = DB::Execute('truncate table ' . config('database.prefix') . 'visit_log');
        if ($result !== false) {
            ajax_return_ok([], '清空成功');
        } else {
            ajax_return_error('清空失败');
        }
    }
}
