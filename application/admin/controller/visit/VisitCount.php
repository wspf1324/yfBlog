<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\visit;

use app\admin\controller\Base;
use app\admin\model\visit\VisitCount as VisitCountModel;

class VisitCount extends Base
{

    /**
     * 获取统计列表
     *
     * @route('visits/list')
     */
    public function index()
    {
        $countDate = input('countDate', 'all', 'trim');
        $startTime = input('startTime', '', 'strtotime');
        $endTime = input('endTime', '', 'strtotime');
        $sort = input('sort', 'id desc', 'trim');
        $page = input('page', 1, 'intval');
        $psize = input('psize', 10, 'intval');
        $lists = VisitCountModel::getLists($countDate, $startTime, $endTime, $sort, $page, $psize);
        $result['total'] = VisitCountModel::getTotal($countDate, $startTime, $endTime);
        $result['data'] = $lists;
        ajax_return_ok($result);
    }
}
