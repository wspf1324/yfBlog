<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\setting;

use app\admin\model\setting\AuthGroup as AuthGroupModel;
use app\admin\controller\Base;

/**
 * 角色管理
 * @author rainfer520@qq.com
 *
 */
class Roles extends Base
{
    /**
     * 列表
     *
     * @route('roles/list')
     */
    public function index()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $title = input('title', '', 'trim');
            $status = input('status', -1, 'int');
            $order = input('order/a', '');
            $page = input('page', 1, 'intval');
            $psize = input('psize', 10, 'intval');

            $lists = AuthGroupModel::getLists($title, $status, $order, $page, $psize);
            $result['total'] = AuthGroupModel::getTotal($title, $status);
            $result['data'] = $lists;
            ajax_return_ok($result);
        }
    }

    /**
     * 列表,不分页
     *
     * @route('roles/getAll')
     */
    public function getAll()
    {
        if ($this->request->isPost()) {
            $lists = AuthGroupModel::getListsAll(1);
            ajax_return_ok($lists);
        }
    }


    /** 详情
     *
     * @route('roles/getInfo')
     */
    public function getInfo()
    {
        $id = input('id', 0, 'intval');
        if ($id == 0) {
            ajax_return_error('参数有误！');
        }
        $info = AuthGroupModel::find($id);
        ajax_return_ok($info);
    }

    /**
     * 保存
     *
     * @throws
     * @route('roles/save')
     */
    public function save()
    {
        $id = input('id', '0', 'intval');
        //接收数据
        $data = [
            'title' => input('title', '', 'trim'),
            'status' => input('status', 0, 'intval'),
            'rules' => input('rules/a', '')
        ];
        $validate = validate('setting.AuthGroup');
        $result = $validate->check($data);
        if (!$result) {
            $error = $validate->getError();
            ajax_return_error($error);
        }
        $data['rules'] = implode(',', $data['rules']);
        if ($id) {
            $res = AuthGroupModel::where('id', $id)->update($data);
        } else {
            $res = AuthGroupModel::create($data);
            $id = $res ? $res->id : 0;
        }
        if ($res !== false) {
            ajax_return_ok(['id' => $id], '保存成功！');
        } else {
            ajax_return_error('保存失败！');
        }
    }

    /**
     * 删除
     *
     * @route('roles/del')
     */
    public function del()
    {
        $id = input('id', 0, 'intval');
        if ($id == 0) {
            ajax_return_error('参数有误！');
        } else {
            if ($id == 1) {
                ajax_return_error('该记录不能删除！');
            }
            if (AuthGroupModel::destroy($id)) {
                ajax_return_ok([], '删除成功！');
            } else {
                ajax_return_error('删除失败！');
            }
        }
    }

    /**
     * 批量删除
     *
     * @route('roles/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            $ids = explode(',', $ids);
            $ids = array_diff($ids, [1]);
            AuthGroupModel::destroy($ids);
            ajax_return_ok([], '删除成功！');
        }
    }

    /**
     * 修改
     *
     * @throws
     * @route('roles/change')
     */
    public function change()
    {

        $val = input('val', '', 'int');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($val) || empty($field)) {
            ajax_return_error('参数有误！');
        }
        $res = AuthGroupModel::where('id', $val)->update([$field => $value]);
        if ($res !== false) {
            ajax_return_ok([], '修改成功！');
        } else {
            ajax_return_error('修改失败！');
        }

    }

    /**
     * 批量修改
     *
     * @throws
     * @route('roles/changeAll')
     */
    public function changeAll()
    {
        $val = input('val', '', 'trim');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'intval');
        if (empty($val) || empty($field)) {
            ajax_return_error('参数有误！');
        }

        $ids = explode(',', $val);
        $lists = [];
        foreach ($ids as $v) {
            $lists[] = ['id' => $v, $field => $value];
        }
        $authGroup = new AuthGroupModel;
        $authGroup->saveAll($lists);
        ajax_return_ok([], '修改成功！');

    }


}
