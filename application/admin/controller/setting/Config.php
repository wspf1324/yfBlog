<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\setting;

use app\admin\controller\Base;
use app\admin\model\setting\Config as ConfigModel;

class Config extends Base
{
    /**
     * 获取配置
     *
     * @return mixed
     * @route('setting/getConfig')
     */
    public function getConfig()
    {
        $group = input('group', 'basic', 'trim');
        $list = ConfigModel::getAll($group);
        ajax_return_ok($list);
    }

    /**
     * 设置配置
     *
     * @return mixed
     * @throws
     * @route('setting/setConfig')
     */
    public function setConfig()
    {
        $posts = input('post.');
        foreach ($posts as $key => $value) {
            ConfigModel::where('name', $key)->update(['value'=>$value]);
        }
        ajax_return_ok([], '保存成功');
    }
}
