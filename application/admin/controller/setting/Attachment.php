<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\setting;

use app\admin\controller\Base;
use app\admin\model\setting\Attachment as AttachmentModel;

class Attachment extends Base
{
    /**
     * 列表
     *
     * @route('attachment/list')
     */
    public function index()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $name = input('name', '', 'trim');
            $attType = input('attType', '', 'trim');
            $startTime = input('startTime', '', 'strtotime');
            $endTime = input('endTime', '', 'strtotime');
            $page = input('page', 1, 'intval');
            $psize = input('psize', 10, 'intval');

            $lists = AttachmentModel::getLists($name, $attType, $startTime, $endTime, $page, $psize);
            $result['total'] = AttachmentModel::getTotal($name, $attType, $startTime, $endTime);
            $result['data'] = $lists;
            ajax_return_ok($result);
        }
    }

    /**
     * 删除
     *
     * @route('attachment/del')
     */
    public function del()
    {
        $id = input('id', 0, 'intval');
        if ($id == 0) {
            ajax_return_error('参数有误！');
        } else {
            if (AttachmentModel::destroy($id)) {
                ajax_return_ok([], '删除成功！');
            } else {
                ajax_return_error('删除失败！');
            }
        }
    }

    /**
     * 批量删除
     *
     * @route('attachment/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            $ids = explode(',', $ids);
            AttachmentModel::destroy($ids);
            ajax_return_ok([], '删除成功！');
        }
    }
}
