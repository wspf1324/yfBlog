<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller\setting;

use app\admin\model\setting\AuthRule as AuthRuleModel;
use app\admin\controller\Base;

/**
 * 规则管理
 * @author rainfer520@qq.com
 */
class Rules extends Base
{
    /**
     * 列表
     *
     * @route('rules/list')
     */
    public function index()
    {
        if ($this->request->isPost()) {
            //搜索参数
            $title = input('title', '', 'trim');
            $status = input('status', -1, 'int');
            $order = input('order/a', 'sorts');
            $lists = AuthRuleModel::getLists($title, $status, $order);
            $result['data'] = $lists;
            ajax_return_ok($result);
        }
    }

    /**
     * 列表,不分页
     *
     * @route('rules/getLists')
     */
    public function getLists()
    {
        if ($this->request->isPost()) {
            $lists = AuthRuleModel::getListsAll(1);
            ajax_return_ok($lists);
        }
    }


    /**
     * 详情
     *
     * @route('rules/getInfo')
     */
    public function getInfo()
    {
        $id = input('id', 0, 'intval');
        if ($id == 0) {
            ajax_return_error('参数有误！');
        }
        $info = AuthRuleModel::find($id);
        ajax_return_ok($info);
    }

    /**
     * 保存
     *
     * @throws
     * @route('rules/save')
     */
    public function save()
    {
        $id = input('id', 0, 'intval');
        //接收数据
        $data = [
            'pid' => input('pid', 0, 'int'),
            'title' => input('title', '', 'trim'),
            'name' => input('name', '', 'trim'),
            'icon' => input('icon', '', 'trim'),
            'status' => input('status', 0, 'int'),
            'sorts' => input('sorts', 0, 'int'),
            'path' => input('path', '', 'trim'),
            'component' => input('component', '', 'trim'),
            'redirect' => input('redirect', '', 'trim'),
            'hidden' => input('hidden', 0, 'int'),
            'noCache' => input('noCache', 1, 'int'),
            'alwaysShow' => input('alwaysShow', 1, 'int')
        ];
        $validate = validate('setting.AuthRule');
        $result = $validate->check($data);
        if (!$result) {
            $error = $validate->getError();
            ajax_return_error($error);
        }

        if ($id && $id == $data['pid']) {
            ajax_return_error('上级不能选择自己！');
        }
        if ($id) {
            $res = AuthRuleModel::where('id', $id)->update($data);
        } else {
            $res = AuthRuleModel::create($data);
            if ($res) {
                $id = $res->id;
            }
        }
        if ($res !== false) {
            ajax_return_ok(['id' => $id], '保存成功！');
        } else {
            ajax_return_error('保存失败！');
        }
    }

    /**
     * 删除
     *
     * @route('rules/del')
     */
    public function del()
    {
        $id = input('id', 0, 'intval');
        if ($id == 0) {
            ajax_return_error('参数有误！');
        } else {
            if (AuthRuleModel::destroy($id)) {
                ajax_return_ok([], '删除成功！');
            } else {
                ajax_return_error('删除失败！');
            }
        }
    }

    /**
     * 修改
     *
     * @throws
     * @route('rules/change')
     */
    public function change()
    {
        $val = input('val', 0, 'intval');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($field) || $val == 0) {
            ajax_return_error('参数有误！');
        }
        $res = AuthRuleModel::where('id', $val)->update([$field => $value]);
        if ($res !== false) {
            ajax_return_ok([], '修改成功！');
        } else {
            ajax_return_error('修改失败！');
        }

    }

    /**
     * 排序
     *
     * @throws
     * @route('rules/sort')
     */
    public function sort()
    {
        $listOrder = input('listOrder/a');
        if (empty($listOrder)) {
            ajax_return_error('参数有误！');
        }
        $lists = [];
        foreach ($listOrder as $id => $sorts) {
            $lists[] = ['id' => $id, 'sorts' => $sorts];
        }
        $authRule = new AuthRuleModel;
        $authRule->saveAll($lists);
        ajax_return_ok([], '排序成功！');
    }

    /**
     * 批量删除
     *
     * @route('rules/delAll')
     */
    public function delAll()
    {
        $ids = input('ids', '', 'trim');
        if (empty($ids)) {
            ajax_return_error('参数有误！');
        } else {
            $ids = explode(',', $ids);
            AuthRuleModel::destroy($ids);
            ajax_return_ok([], '删除成功！');
        }
    }

    /**
     *
     * @throws
     * @route('rules/changeAll')
     */
    public function changeAll()
    {
        $val = input('val', '', 'trim');
        $field = input('field', '', 'trim');
        $value = input('value', '', 'int');
        if (empty($val) || empty($field)) {
            ajax_return_error('参数有误！');
        }
        $lists = [];
        $ids = explode(',', $val);
        foreach ($ids as $v) {
            $lists[] = ['id' => $v, $field => $value];
        }
        $authRule = new AuthRuleModel;
        $authRule->saveAll($lists);
        ajax_return_ok([], '修改成功！');

    }

}
