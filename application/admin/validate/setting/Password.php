<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\validate\setting;

/**
 * 密码修改
 */
class Password extends \think\Validate
{
    //验证规则
    protected $rule = [
        'oldPwd' => 'require',
        'newPwd' => 'require|length:6,10',
        'newPwd2' => 'require|confirm:newPwd',
    ];

    //提示信息
    protected $message = [
        'oldPwd' => '原始密码必填',
        'newPwd.require' => '新密码必填',
        'newPwd.length' => '密码长度为6到10位',
        'newPwd2.require' => '确认密码必填',
        'newPwd2.confirm' => '新密码不相等',
    ];
}
