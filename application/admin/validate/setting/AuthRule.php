<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\validate\setting;

/**
 * 规则
 */
class AuthRule extends \think\Validate
{
    //验证规则
    protected $rule = [
        'title' => ['require'],
        'name' => ['require'],
        'icon' => ['require'],
        'path' => ['require'],
        'component' => ['require'],
    ];

    //提示信息
    protected $message = [
        'title' => '名称必填',
        'name' => '标识必填',
        'icon' => '图标必填',
        'path' => '路径必填',
        'component' => '组件必填',
    ];
}
