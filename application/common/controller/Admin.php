<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

namespace app\common\controller;

use app\admin\model\setting\AuthRule as AuthRuleModel;
use AuthUtil;

/**
 * 后台接口基类
 * @author rainfer520@qq.com
 *
 */
class Admin extends Api
{
    //用户信息
    protected $user = [];
    protected $uid = 0;

    public function initialize()
    {
        parent::initialize();
        //身份验证
        $result = AuthUtil::checkUser('admin');
        if ($result['status']) {
            $this->user = $result['msg'];
            $this->uid = $result['msg']['id'];
        }
        //权限验证
        $module = strtolower(request()->module());
        $controller = strtolower(request()->controller());
        $action = strtolower(request()->action());
        $nowUrl = $module . '/' . $controller . '/' . $action;
        $access = AuthRuleModel::hasAccessByName($nowUrl, $result['msg']['groupId']);
        if (!$access) {
            ajax_return_error('无权访问！');
        }
    }
}
