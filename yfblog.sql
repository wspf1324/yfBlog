SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yf_admin
-- ----------------------------
DROP TABLE IF EXISTS `yf_admin`;
CREATE TABLE `yf_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `passwordSalt` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '盐',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户邮箱',
  `realName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户手机',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `regTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
  `regIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '注册IP',
  `loginTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `loginIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `updateTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `isEnabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '用户状态  0 禁用，1正常',
  `groupId` mediumint(8) NOT NULL DEFAULT 0 COMMENT '权限组',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`userName`) USING BTREE,
  INDEX `status`(`isEnabled`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_admin
-- ----------------------------
INSERT INTO `yf_admin` VALUES (1, 'admin', '7dc640d42f444d94962efae359b63898', 'adeudiudie', '123@163.com', '123', '', 'http://www.yfblog.com/uploads/images/20200114/e7a6dfb2b360176a344ed3b3eb14660f.png', 1498276451, '127.0.0.1', 1580719786, '39.149.12.184', 1580719786, 1, 1);

-- ----------------------------
-- Table structure for yf_app
-- ----------------------------
DROP TABLE IF EXISTS `yf_app`;
CREATE TABLE `yf_app`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `appId` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用id',
  `appSecret` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用密钥',
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `regTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
  `regIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '注册IP',
  `loginTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `loginIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `updateTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `isEnabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '用户状态  0 禁用，1正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'app应用表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_app
-- ----------------------------
INSERT INTO `yf_app` VALUES (1, 'ty9fd2848a039ab554', 'ec32286d0718118861afdbf6e401ee81', '管理员端', '', 1498276451, '127.0.0.1', 1521305444, '123.149.208.76', 1514962598, 1);

-- ----------------------------
-- Table structure for yf_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_group`;
CREATE TABLE `yf_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '为1正常，为0禁用',
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户组拥有的规则id， 多个规则\",\"隔开',
  `updateTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_auth_group
-- ----------------------------
INSERT INTO `yf_auth_group` VALUES (1, '超级管理员', 1, '39,40,58,59,42,1,2,7,38,43,44,53,54,55,56,57,65,66', 1552300913);
INSERT INTO `yf_auth_group` VALUES (2, '普通管理员', 1, '39,40,50,51,52,53,54,55', 1542787522);

-- ----------------------------
-- Table structure for yf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_rule`;
CREATE TABLE `yf_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '为1正常，为0禁用',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 正常，0=禁用',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证',
  `pid` mediumint(8) NOT NULL DEFAULT 0 COMMENT '上级菜单',
  `sorts` int(8) NOT NULL DEFAULT 0 COMMENT '升序',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '图标',
  `updateTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路经',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路由页面地址',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT '左侧菜单 0==显示,1隐藏',
  `noCache` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1=不缓存，0=缓存',
  `alwaysShow` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1= 总显示,0=否 依据子菜单个数',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '重定向地址',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_auth_rule
-- ----------------------------
INSERT INTO `yf_auth_rule` VALUES (1, 'manage', '权限管理', 1, 1, '', 42, 1, 'component', 1547187800, 'manage', 'setting/manage/index', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (2, 'manage/admin', '管理员列表', 1, 1, '', 1, 0, 'user', 1541666364, 'admin', 'setting/manage/admin/admin', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (7, 'manage/rules', '权限列表', 1, 1, '', 1, 0, 'lock', 1542353476, 'rules', 'setting/manage/rules/rules', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (38, 'manage/roles', '角色列表', 1, 1, '', 1, 0, 'list', 1542602805, 'roles', 'setting/manage/roles/roles', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (39, 'log', '日志管理', 1, 1, '', 0, 20, 'component', 1542602916, '/log', 'layout/Layout', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (40, 'log/log', '登陆日志', 1, 1, '', 39, 0, 'list', 1552301777, 'log', 'log/log', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (42, 'setting', '系统管理', 1, 1, '', 0, 30, 'setting', 0, '/setting', 'layout/Layout', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (43, 'setting/system', '系统设置', 1, 1, '', 42, 0, 'setting', 0, 'system', 'setting/system', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (44, 'attachment', '附件管理', 1, 1, '', 42, 0, 'documentation', 0, 'attachment', 'setting/attachment', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (50, 'user', '会员管理', 1, 0, '', 0, 15, 'users', 0, '/user', 'layout/Layout', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (51, 'user/user', '会员列表', 1, 1, '', 50, 0, 'users', 0, 'user', 'user/user', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (52, 'user/message', '站内信息', 1, 1, '', 50, 0, 'notice', 0, 'message', 'user/message', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (53, 'blog', '文章管理', 1, 1, '', 0, 5, 'component', 0, '/blog', 'layout/Layout', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (54, 'blog/category', '文章分类', 1, 1, '', 53, 0, 'fenlei', 0, 'category', 'blog/category', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (55, 'blog/blog', '文章列表', 1, 1, '', 53, 0, 'blog', 0, 'blog', 'blog/blog', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (56, 'tag', '标签管理', 1, 1, '', 0, 10, 'biaoqian', 0, '/tag', 'layout/Layout', 0, 0, 1, '');
INSERT INTO `yf_auth_rule` VALUES (57, 'tag/tag', '标签列表', 1, 1, '', 56, 0, 'biaoqian', 0, 'tag', 'tag/tag', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (58, 'log/visit', '访问日志', 1, 1, '', 39, 5, 'visits', 1552301777, 'visit', 'log/visit', 0, 1, 1, '');
INSERT INTO `yf_auth_rule` VALUES (59, 'log/visits', '访问统计', 1, 1, '', 39, 0, 'chart', 1552301777, 'visits', 'log/visits', 0, 1, 1, '');

-- ----------------------------
-- Table structure for yf_blog
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog`;
CREATE TABLE `yf_blog`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章管理ID',
  `cid` int(10) NOT NULL DEFAULT 0 COMMENT '分类id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章标题',
  `stitle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '短标题',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '关键词(多个逗号隔开)',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文章作者',
  `synopsis` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文章简介',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文章图片',
  `visit` int(10) DEFAULT 0 COMMENT '浏览次数',
  `sort` int(10) UNSIGNED DEFAULT 30 COMMENT '排序',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '原文链接',
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '来源',
  `addTime` int(10) NOT NULL COMMENT '添加时间',
  `displayTime` int(10) DEFAULT 0 COMMENT '显示时间',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态(0=未审核 1审核)',
  `isDel` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '是否删除',
  `isHot` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '是否热门',
  `isBanner` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '是否轮播图',
  `adminId` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章管理表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_blog
-- ----------------------------
INSERT INTO `yf_blog` VALUES (1, 1, '测试 ddd', '', '地点,哒哒哒,asdfsd,asdf地点', '123', '大撒旦', 'http://datong.crmeb.net/public/uploads/editor/20190115/5c3dc294a9a0a.jpg', 143, 0, '', '', 1546054151, 1548138209, 1, 0, 1, 1, 1);
INSERT INTO `yf_blog` VALUES (2, 2, '阿斯达斯', '', '', '123', '阿斯达', 'http://datong.crmeb.net/public/uploads/editor/20190115/5c3dc294a9a0a.jpg', 63, 0, '', '', 1552965683, 1552965683, 1, 0, 0, 0, 1);
INSERT INTO `yf_blog` VALUES (3, 4, '111', '', '', '123', '', 'http://www.yfblog.com/uploads/images/20200116/a2fad1772eee6c957479799411b42db3.png', 0, 0, '', '', 0, 1577594456, 1, 0, 0, 0, 1);
INSERT INTO `yf_blog` VALUES (4, 3, '222', '', '', '123', '', 'http://www.yfblog.com/uploads/images/20200116/a2fad1772eee6c957479799411b42db3.png', 10, 0, '', '', 0, 1577594467, 1, 0, 0, 0, 1);
INSERT INTO `yf_blog` VALUES (5, 5, '333', '', '', '123', '', 'http://www.yfblog.com/uploads/images/20200116/1f8cb6e864ca685d01dfb0265582a3d5.png,http://www.yfblog.com/uploads/images/20200116/d91c71bcfe9df196863b1b92633d97c1.png', 0, 0, '', '', 0, 1577594499, 1, 0, 0, 0, 1);
INSERT INTO `yf_blog` VALUES (6, 1, '111', '', '', '123', '', '', 0, 0, '', '', 1580128588, 1580128569, 1, 0, 0, 0, 1);
INSERT INTO `yf_blog` VALUES (7, 1, 'dddd', '', '', '123', '', '', 0, 0, '', '', 1580282697, 1580282660, 1, 0, 0, 0, 1);

-- ----------------------------
-- Table structure for yf_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog_category`;
CREATE TABLE `yf_blog_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章分类id',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章分类标题',
  `intr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文章分类简介',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文章分类图片',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 30 COMMENT '排序',
  `isDel` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1删除0未删除',
  `addTime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否隐藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_blog_category
-- ----------------------------
INSERT INTO `yf_blog_category` VALUES (1, 0, '前 端', '前端技术', '', 1, 0, 0, 1548138187, 0);
INSERT INTO `yf_blog_category` VALUES (2, 0, 'PHP', 'PHP技术', '', 1, 1, 0, 1554889503, 0);
INSERT INTO `yf_blog_category` VALUES (3, 0, 'Java', 'Java技术', '', 1, 2, 0, 1554889781, 0);
INSERT INTO `yf_blog_category` VALUES (4, 0, '算法与结构', '算法与结构', '', 1, 3, 0, 1548138187, 0);
INSERT INTO `yf_blog_category` VALUES (5, 0, '随笔', '随笔', '', 1, 4, 0, 1554889503, 0);

-- ----------------------------
-- Table structure for yf_blog_comments
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog_comments`;
CREATE TABLE `yf_blog_comments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `bid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章id',
  `uid` int(11) DEFAULT 0 COMMENT '发表评论的用户id',
  `toUid` int(11) DEFAULT 0 COMMENT '被评论的用户id',
  `fullName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '评论者昵称',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '评论者邮箱',
  `addTime` int(10) NOT NULL DEFAULT 0 COMMENT '评论时间',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论内容',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '评论类型；1用户评论 0游客评论',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '被回复的评论id',
  `path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '层次',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态，1已审核，0未审核',
  `isDel` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `comment_post_ID`(`bid`) USING BTREE,
  INDEX `comment_approved_date_gmt`(`status`) USING BTREE,
  INDEX `comment_parent`(`pid`) USING BTREE,
  INDEX `table_id_status`(`bid`, `status`) USING BTREE,
  INDEX `createtime`(`addTime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for yf_blog_content
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog_content`;
CREATE TABLE `yf_blog_content`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `nid` int(10) UNSIGNED NOT NULL COMMENT '文章id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章内容',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `nid`(`nid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章内容表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_blog_content
-- ----------------------------
INSERT INTO `yf_blog_content` VALUES (1, 1, '<p><span style=\"font-size: 12pt;\">应用场景：需要动态显示进度条，在后台管理系统的仪表盘经常用到</span></p>\n<p><span style=\"font-size: 12pt;\">思路：基于element UI的progress，通过动态改变<strong>percentage</strong>来达到效果</span></p>\n<p><span style=\"font-size: 12pt;\">为了提高性能，首先想到的是利用浏览器的web API 的<strong><code>requestAnimationFrame</code></strong>另外，需要考虑浏览器兼容性，及老版本的浏览器不支持这个API的需要使用<strong>setTimeout</strong></span></p>\n<blockquote>\n<pre><span style=\"font-size: 12pt;\"><strong>requestAnimationFrame.js</strong></span></pre>\n</blockquote>\n<pre class=\"language-javascript\"><code>let lastTime = 0\nconst prefixes = \'webkit moz ms o\'.split(\' \') // 各浏览器前缀\n\nlet requestAnimationFrame\nlet cancelAnimationFrame\n\nconst isServer = typeof window === \'undefined\'\nif (isServer) {\nrequestAnimationFrame = function() {\nreturn\n}\ncancelAnimationFrame = function() {\nreturn\n}\n} else {\nrequestAnimationFrame = window.requestAnimationFrame\ncancelAnimationFrame = window.cancelAnimationFrame\nlet prefix\n// 通过遍历各浏览器前缀，来得到requestAnimationFrame和cancelAnimationFrame在当前浏览器的实现形式\nfor (let i = 0; i &lt; prefixes.length; i++) {\nif (requestAnimationFrame &amp;&amp; cancelAnimationFrame) { break }\nprefix = prefixes[i]\nrequestAnimationFrame = requestAnimationFrame || window[prefix + \'RequestAnimationFrame\']\ncancelAnimationFrame = cancelAnimationFrame || window[prefix + \'CancelAnimationFrame\'] || window[prefix + \'CancelRequestAnimationFrame\']\n}\n\n// 如果当前浏览器不支持requestAnimationFrame和cancelAnimationFrame，则会退到setTimeout\nif (!requestAnimationFrame || !cancelAnimationFrame) {\nrequestAnimationFrame = function(callback) {\nconst currTime = new Date().getTime()\n// 为了使setTimteout的尽可能的接近每秒60帧的效果\nconst timeToCall = Math.max(0, 16 - (currTime - lastTime))\nconst id = window.setTimeout(() =&gt; {\ncallback(currTime + timeToCall)\n}, timeToCall)\nlastTime = currTime + timeToCall\nreturn id\n}\n\ncancelAnimationFrame = function(id) {\nwindow.clearTimeout(id)\n}\n}\n}\n\nexport { requestAnimationFrame, cancelAnimationFrame }</code></pre>\n<p><span style=\"font-size: 12pt;\">然后建立组件index.vue</span></p>\n<pre class=\"language-markup\"><code>&lt;el-progress\n:type=\"type\"\n:status=\"status\"\n:stroke-width=\"strokeWidth\"\n:stroke-linecap=\"strokeLinecap\"\n:text-inside=\"textInside\"\n:width=\"width\"\n:show-text=\"showText\"\n:color=\"color\"\n:percentage=\"displayValue\"\n/&gt;</code></pre>\n<p><span style=\"font-size: 12pt;\">使用：</span></p>\n<blockquote>\n<pre><span style=\"font-size: 12pt;\">Js部分：</span></pre>\n</blockquote>\n<pre class=\"language-javascript\"><code>import { requestAnimationFrame, cancelAnimationFrame } from \'./requestAnimationFrame.js\'\nexport default {\n  props: {\n    type: {\n      type: String,\n      default: \'line\',\n      validator: val =&gt; [\'line\', \'circle\', \'dashboard\'].indexOf(val) &gt; -1\n    },\n    status: {\n      type: String,\n      default: \'\',\n      validator: val =&gt; [\'success\', \'exception\', \'warning\'].indexOf(val) &gt; -1\n    },\n    strokeWidth: {\n      type: Number,\n      default: 6\n    },\n    strokeLinecap: {\n      type: String,\n      default: \'round\'\n    },\n    textInside: {\n      type: Boolean,\n      default: false\n    },\n    width: {\n      type: Number,\n      default: 126\n    },\n    showText: {\n      type: Boolean,\n      default: true\n    },\n    color: {\n      type: [String, Array, Function],\n      default: \'\'\n    },\n    startVal: {\n      type: Number,\n      required: false,\n      default: 0\n    },\n    endVal: {\n      type: Number,\n      required: false,\n      default: 2017\n    },\n    duration: {\n      type: Number,\n      required: false,\n      default: 3000\n    },\n    autoplay: {\n      type: Boolean,\n      required: false,\n      default: true\n    },\n    decimals: {\n      type: Number,\n      required: false,\n      default: 0,\n      validator(value) {\n        return value &gt;= 0\n      }\n    },\n    decimal: {\n      type: String,\n      required: false,\n      default: \'.\'\n    },\n    separator: {\n      type: String,\n      required: false,\n      default: \',\'\n    },\n    prefix: {\n      type: String,\n      required: false,\n      default: \'\'\n    },\n    suffix: {\n      type: String,\n      required: false,\n      default: \'\'\n    },\n    useEasing: {\n      type: Boolean,\n      required: false,\n      default: true\n    },\n    easingFn: {\n      type: Function,\n      default(t, b, c, d) {\n        return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b\n      }\n    }\n  },\n  data() {\n    return {\n      localStartVal: this.startVal,\n      displayValue: this.formatNumber(this.startVal),\n      printVal: null,\n      paused: false,\n      localDuration: this.duration,\n      startTime: null,\n      timestamp: null,\n      remaining: null,\n      rAF: null\n    }\n  },\n  computed: {\n    countDown() {\n      return this.startVal &gt; this.endVal\n    }\n  },\n  watch: {\n    startVal() {\n      if (this.autoplay) {\n        this.start()\n      }\n    },\n    endVal() {\n      if (this.autoplay) {\n        this.start()\n      }\n    }\n  },\n  mounted() {\n    if (this.autoplay) {\n      this.start()\n    }\n    this.$emit(\'mountedCallback\')\n  },\n  destroyed() {\n    cancelAnimationFrame(this.rAF)\n  },\n  methods: {\n    start() {\n      this.localStartVal = this.startVal\n      this.startTime = null\n      this.localDuration = this.duration\n      this.paused = false\n      this.rAF = requestAnimationFrame(this.count)\n    },\n    pauseResume() {\n      if (this.paused) {\n        this.resume()\n        this.paused = false\n      } else {\n        this.pause()\n        this.paused = true\n      }\n    },\n    pause() {\n      cancelAnimationFrame(this.rAF)\n    },\n    resume() {\n      this.startTime = null\n      this.localDuration = +this.remaining\n      this.localStartVal = +this.printVal\n      requestAnimationFrame(this.count)\n    },\n    reset() {\n      this.startTime = null\n      cancelAnimationFrame(this.rAF)\n      this.displayValue = this.formatNumber(this.startVal)\n    },\n    count(timestamp) {\n      if (!this.startTime) this.startTime = timestamp\n      this.timestamp = timestamp\n      const progress = timestamp - this.startTime\n      this.remaining = this.localDuration - progress\n\n      if (this.useEasing) {\n        if (this.countDown) {\n          this.printVal = this.localStartVal - this.easingFn(progress, 0, this.localStartVal - this.endVal, this.localDuration)\n        } else {\n          this.printVal = this.easingFn(progress, this.localStartVal, this.endVal - this.localStartVal, this.localDuration)\n        }\n      } else {\n        if (this.countDown) {\n          this.printVal = this.localStartVal - ((this.localStartVal - this.endVal) * (progress / this.localDuration))\n        } else {\n          this.printVal = this.localStartVal + (this.endVal - this.localStartVal) * (progress / this.localDuration)\n        }\n      }\n      if (this.countDown) {\n        this.printVal = this.printVal &lt; this.endVal ? this.endVal : this.printVal\n      } else {\n        this.printVal = this.printVal &gt; this.endVal ? this.endVal : this.printVal\n      }\n\n      this.displayValue = this.formatNumber(this.printVal)\n      if (progress &lt; this.localDuration) {\n        this.rAF = requestAnimationFrame(this.count)\n      } else {\n        this.$emit(\'callback\')\n      }\n    },\n    isNumber(val) {\n      return !isNaN(parseFloat(val))\n    },\n    formatNumber(num) {\n      num = num.toFixed(this.decimals)\n      num += \'\'\n      const x = num.split(\'.\')\n      let x1 = x[0]\n      const x2 = x.length &gt; 1 ? this.decimal + x[1] : \'\'\n      const rgx = /(\\d+)(\\d{3})/\n      if (this.separator &amp;&amp; !this.isNumber(this.separator)) {\n        while (rgx.test(x1)) {\n          x1 = x1.replace(rgx, \'$1\' + this.separator + \'$2\')\n        }\n      }\n      return this.prefix + x1 + x2 + this.suffix\n    }\n  }\n}</code></pre>\n<p><span style=\"font-size: 12pt;\">效果：<a title=\"动态进度条\" href=\"#/resume\" target=\"_blank\" rel=\"noopener\">http://blog.rainfer.cn/#/resume</a></span></p>');
INSERT INTO `yf_blog_content` VALUES (2, 2, '<p>阿斯打扫打扫的阿斯达傻吊</p>');
INSERT INTO `yf_blog_content` VALUES (3, 3, '测试数据');
INSERT INTO `yf_blog_content` VALUES (4, 4, '测试数据');
INSERT INTO `yf_blog_content` VALUES (5, 5, '测试数据');
INSERT INTO `yf_blog_content` VALUES (6, 6, '<div class=\"s-news-special s-news-item s-news-special-item-tpl-3 s-opacity-blank8 first\" data-url=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9942520119724833884%22%7D&amp;n_type=0&amp;p_from=1\" data-rid=\"9942520119724833884\" data-relatewords=\"1\" data-log=\"stype:0;picNum:0;title:华为：向武汉市慈善总会账户捐赠3000万人民币，用于疫情防控;pos:0;st:news;\" data-extra=\"{&quot;ac&quot;:0,&quot;attnua&quot;:0,&quot;category&quot;:1,&quot;channel_id&quot;:1,&quot;city&quot;:&quot;\\u840d\\u4e61&quot;,&quot;cs&quot;:&quot;73366620 2314923359&quot;,&quot;ctr&quot;:1,&quot;dattwei&quot;:0,&quot;dbaswei&quot;:0,&quot;disp_log_qid&quot;:3097346160,&quot;district&quot;:&quot;&quot;,&quot;dscale&quot;:5,&quot;event_hot&quot;:0,&quot;gbdt_score&quot;:0,&quot;high_level_flag&quot;:0,&quot;important_news&quot;:0,&quot;intervene_level&quot;:0,&quot;is_tab_recommend&quot;:0,&quot;low_quality_sensin_ctr&quot;:-1,&quot;manual_edit&quot;:0,&quot;mark&quot;:0,&quot;mark_rec&quot;:0,&quot;mthid&quot;:&quot;1559833477259726&quot;,&quot;new_cat&quot;:&quot;\\u79d1\\u6280&quot;,&quot;new_sub_cat&quot;:&quot;\\u534e\\u4e3a\\u624b\\u673a&quot;,&quot;offline_rank_score&quot;:0,&quot;poi_type&quot;:1000,&quot;predictor_extmsg&quot;:&quot;&quot;,&quot;predictor_result&quot;:&quot;&quot;,&quot;province&quot;:&quot;\\u6c5f\\u897f&quot;,&quot;public_time&quot;:1580057104,&quot;r_att_src&quot;:3,&quot;rec_src&quot;:[2],&quot;rec_tabs&quot;:&quot;&quot;,&quot;recall_att_click&quot;:23,&quot;recall_attention&quot;:&quot;\\u534e\\u4e3a&quot;,&quot;recall_type&quot;:3,&quot;refresh_timestamp_ms&quot;:1580127966976,&quot;rela_score&quot;:0,&quot;score&quot;:3.5837549758685,&quot;srccat&quot;:0,&quot;srcid&quot;:80001,&quot;tag&quot;:0,&quot;tj_timeliness&quot;:1,&quot;ua&quot;:&quot;&quot;,&quot;uattwei&quot;:1.594,&quot;user_active&quot;:-1,&quot;user_hotnews_level&quot;:0,&quot;user_profile&quot;:0,&quot;userdocwei&quot;:0,&quot;ut&quot;:&quot;&quot;,&quot;vertical_type&quot;:5}\">\n<h2><a class=\"s-title-yahei\" title=\"华为：向武汉市慈善总会账户捐赠3000万人民币，用于疫情防控\" href=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9942520119724833884%22%7D&amp;n_type=0&amp;p_from=1\" target=\"_blank\" rel=\"noopener\" data-title=\"华为：向武汉市慈善总会账户捐赠3000万人民币，用于疫情防控\" data-link=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9942520119724833884%22%7D&amp;n_type=0&amp;p_from=1\" data-click=\"LOG_LINK\">华为：向武汉市慈善总会账户捐赠3000万人民币，用于疫情防控</a></h2>\n<div class=\"from\"><span class=\"src-net\"><a href=\"http://baijiahao.baidu.com/\" target=\"_blank\" rel=\"noopener\" data-src=\"1\" data-click=\"LOG_LINK\">创业邦传媒&nbsp;</a></span><span class=\"src-time\">01-27 00:45</span></div>\n</div>\n<div class=\"s-news-special s-news-item s-news-special-item-tpl-2 s-opacity-blank8\" data-url=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9717371770674775916%22%7D&amp;n_type=0&amp;p_from=1\" data-rid=\"9717371770674775916\" data-relatewords=\"1\" data-log=\"stype:1;picNum:1;title:全国确诊感染新型冠状病毒的肺炎病例共计2835例;pos:1;st:news;pos-0:1;\" data-extra=\"{&quot;ac&quot;:0,&quot;category&quot;:1,&quot;channel_id&quot;:1,&quot;city&quot;:&quot;\\u840d\\u4e61&quot;,&quot;cs&quot;:&quot;1815789460 104091797&quot;,&quot;district&quot;:&quot;&quot;,&quot;event_hot&quot;:0,&quot;gbdt_score&quot;:0,&quot;high_level_flag&quot;:0,&quot;important_news&quot;:0,&quot;intervene_level&quot;:2116338480,&quot;is_tab_recommend&quot;:0,&quot;low_quality_sensin_ctr&quot;:-1,&quot;manual_edit&quot;:1,&quot;mark&quot;:1,&quot;mark_rec&quot;:1,&quot;mthid&quot;:&quot;1551968238585112&quot;,&quot;new_cat&quot;:&quot;\\u793e\\u4f1a&quot;,&quot;new_sub_cat&quot;:&quot;\\u7efc\\u5408&quot;,&quot;offline_rank_score&quot;:400.25722888413,&quot;predictor_extmsg&quot;:&quot;&quot;,&quot;predictor_result&quot;:&quot;&quot;,&quot;province&quot;:&quot;\\u6c5f\\u897f&quot;,&quot;public_time&quot;:1580125881,&quot;q_ratio&quot;:1,&quot;r_att_src&quot;:1000,&quot;rec_src&quot;:[34],&quot;rec_tabs&quot;:&quot;&quot;,&quot;recall_type&quot;:1,&quot;refresh_timestamp_ms&quot;:1580127966976,&quot;rela_score&quot;:0,&quot;score&quot;:400.25722888413,&quot;sdnn_score&quot;:0,&quot;special_log_flag&quot;:0,&quot;srccat&quot;:0,&quot;srcid&quot;:80001,&quot;tag&quot;:0,&quot;tj_timeliness&quot;:259200,&quot;type&quot;:1,&quot;ua&quot;:&quot;&quot;,&quot;user_active&quot;:-1,&quot;user_hotnews_level&quot;:3,&quot;user_profile&quot;:0,&quot;ut&quot;:&quot;&quot;,&quot;vertical_type&quot;:0}\">\n<div class=\"s-pic-content\">\n<div class=\"s-pic-content-own\">\n<ul class=\"s-news-content-imgs clearfix\">\n<li class=\"item-img-wrap\"><a title=\"全国确诊感染新型冠状病毒的肺炎病例共计2835例\" href=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9717371770674775916%22%7D&amp;n_type=0&amp;p_from=1\" target=\"_blank\" rel=\"noopener\" data-click=\"LOG_LINK\" data-title=\"全国确诊感染新型冠状病毒的肺炎病例共计2835例\"><img class=\"s-news-img\" src=\"https://pics2.baidu.com/feed/6d81800a19d8bc3e5996e7c65116b818a9d34540.jpeg?token=183398ab82114232a9bcec93210dc96a\" width=\"179\" height=\"119\" /></a></li>\n</ul>\n</div>\n</div>\n<div class=\"s-block-container\">\n<div class=\"s-block-container-own\">\n<div class=\"s-text-content\">\n<h2><a class=\"s-title-yahei\" title=\"全国确诊感染新型冠状病毒的肺炎病例共计2835例\" href=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9717371770674775916%22%7D&amp;n_type=0&amp;p_from=1\" target=\"_blank\" rel=\"noopener\" data-title=\"全国确诊感染新型冠状病毒的肺炎病例共计2835例\" data-link=\"https://mbd.baidu.com/newspage/data/landingsuper?context=%7B%22nid%22%3A%22news_9717371770674775916%22%7D&amp;n_type=0&amp;p_from=1\" data-click=\"LOG_LINK\">全国确诊感染新型冠状病毒的肺炎病例共计2835例</a></h2>\n</div>\n<div class=\"from\"><span class=\"src-net\"><a href=\"http://baijiahao.baidu.com/\" target=\"_blank\" rel=\"noopener\" data-src=\"1\" data-click=\"LOG_LINK\">环球时报&nbsp;</a></span><span class=\"src-time\">01-27 19:57</span>&nbsp;<span class=\"hot-point\">热点</span></div>\n</div>\n</div>\n</div>');
INSERT INTO `yf_blog_content` VALUES (7, 7, '<div id=\"BlogPostCategory\">分类:&nbsp;<a href=\"https://www.cnblogs.com/moogle/category/1135001.html\" target=\"_blank\" rel=\"noopener\">javascript</a></div>\n<div id=\"EntryTag\">标签:&nbsp;<a href=\"https://www.cnblogs.com/moogle/tag/tinymce/\">tinymce</a></div>\n<div id=\"blog_post_info\">\n<div id=\"green_channel\"><a id=\"green_channel_digg\"></a>好文要顶&nbsp;<a id=\"green_channel_follow\"></a>关注我&nbsp;<a id=\"green_channel_favorite\"></a>收藏该文&nbsp;<a id=\"green_channel_weibo\" title=\"分享至新浪微博\"></a><img src=\"https://common.cnblogs.com/images/icon_weibo_24.png\" alt=\"\" />&nbsp;<a id=\"green_channel_wechat\" title=\"分享至微信\"></a><img src=\"https://common.cnblogs.com/images/wechat.png\" alt=\"\" /></div>\n</div>');

-- ----------------------------
-- Table structure for yf_blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog_tag`;
CREATE TABLE `yf_blog_tag`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '标签id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签标题',
  `sort` int(10) DEFAULT 30 COMMENT '排序',
  `addTime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 0=未审核 1=审核',
  `isDel` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章标签' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_blog_tag
-- ----------------------------
INSERT INTO `yf_blog_tag` VALUES (1, 'PHP', 30, 1577546838, 1, 0);
INSERT INTO `yf_blog_tag` VALUES (2, 'Java', 30, 1577546838, 1, 0);
INSERT INTO `yf_blog_tag` VALUES (3, '算法', 30, 1577546838, 1, 0);
INSERT INTO `yf_blog_tag` VALUES (4, '排序', 30, 1577546838, 1, 0);
INSERT INTO `yf_blog_tag` VALUES (5, '递归', 30, 1577547056, 1, 0);
INSERT INTO `yf_blog_tag` VALUES (7, '动态进度', 0, 1577891381, 1, 0);

-- ----------------------------
-- Table structure for yf_blog_tag_assoc
-- ----------------------------
DROP TABLE IF EXISTS `yf_blog_tag_assoc`;
CREATE TABLE `yf_blog_tag_assoc`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `nid` int(10) UNSIGNED NOT NULL COMMENT '文章id',
  `tagId` int(10) UNSIGNED NOT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章标签对应表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_blog_tag_assoc
-- ----------------------------
INSERT INTO `yf_blog_tag_assoc` VALUES (1, 1, 1);
INSERT INTO `yf_blog_tag_assoc` VALUES (2, 1, 2);
INSERT INTO `yf_blog_tag_assoc` VALUES (3, 2, 3);
INSERT INTO `yf_blog_tag_assoc` VALUES (4, 1, 3);
INSERT INTO `yf_blog_tag_assoc` VALUES (5, 1, 4);
INSERT INTO `yf_blog_tag_assoc` VALUES (6, 1, 5);
INSERT INTO `yf_blog_tag_assoc` VALUES (7, 1, 7);

-- ----------------------------
-- Table structure for yf_login_log
-- ----------------------------
DROP TABLE IF EXISTS `yf_login_log`;
CREATE TABLE `yf_login_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户ID',
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `loginIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `loginTime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '时间',
  `roles` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `user_name`(`userName`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员登录日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for yf_system_attachment
-- ----------------------------
DROP TABLE IF EXISTS `yf_system_attachment`;
CREATE TABLE `yf_system_attachment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件名称',
  `attDir` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件路径',
  `attSize` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件大小',
  `attType` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件类型',
  `md5` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '附件MD5',
  `createTime` int(11) NOT NULL DEFAULT 0 COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `md5`(`md5`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件管理表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_system_attachment
-- ----------------------------
INSERT INTO `yf_system_attachment` VALUES (1, 'logo.png', '/uploads/images/20191228/024bf1883e56a4f0c38d17edfc719bb3.png', '17493', 'image', '2766f8e50b61b4da9cb47c91d91288d2', 1577530385);

-- ----------------------------
-- Table structure for yf_system_config
-- ----------------------------
DROP TABLE IF EXISTS `yf_system_config`;
CREATE TABLE `yf_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置名',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '配置值',
  `group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置组',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of yf_system_config
-- ----------------------------
INSERT INTO `yf_system_config` VALUES (1, 'siteName', '雨飞博客系统', 'basic');
INSERT INTO `yf_system_config` VALUES (2, 'siteUrl', 'http://www.yfblog.com', 'basic');
INSERT INTO `yf_system_config` VALUES (3, 'siteLogo', 'http:\\/\\/www.yfblog.com\\/uploads\\/images\\/20191228\\/024bf1883e56a4f0c38d17edfc719bb3.png', 'basic');
INSERT INTO `yf_system_config` VALUES (4, 'sitePhone', '', 'basic');
INSERT INTO `yf_system_config` VALUES (5, 'siteEmail', 'rainfer520@qq.com', 'basic');
INSERT INTO `yf_system_config` VALUES (6, 'siteQq', '8181883233', 'basic');

-- ----------------------------
-- Table structure for yf_visit_count
-- ----------------------------
DROP TABLE IF EXISTS `yf_visit_count`;
CREATE TABLE `yf_visit_count`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '访问数据id',
  `countDate` date NOT NULL,
  `total` int(10) NOT NULL DEFAULT 0,
  `human` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `google` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bing` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `baidu` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sogou` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `yisou` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `count_date`(`countDate`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '访问日志统计' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for yf_visit_log
-- ----------------------------
DROP TABLE IF EXISTS `yf_visit_log`;
CREATE TABLE `yf_visit_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `visitor` enum('human','google','bing','baidu','sogou','yisou') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'human',
  `ip` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `visitTime` int(10) DEFAULT 0,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `os` char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `browser` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `from` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `visitor`(`visitor`) USING BTREE,
  INDEX `ip`(`ip`) USING BTREE,
  INDEX `visitTime`(`visitTime`) USING BTREE,
  INDEX `uri`(`uri`) USING BTREE,
  INDEX `visitor_2`(`visitor`, `ip`, `visitTime`, `uri`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '访问日志' ROW_FORMAT = Compact;
