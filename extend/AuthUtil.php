<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

use \think\Db;

/**
 * 权限认证
 * @author rainfer520@qq.com
 *
 */
class AuthUtil
{
    /**
     * 签名验证,校验数据
     *
     * @throws
     */
    public static function checkSign()
    {

        $appid = request()->header('x-access-appid');
        if (empty($appid)) {
            exception('AppId非法');
        }
        // 实时数据
        $app = Db::name('app')->where('appId', $appid)->find();
        //认证：状态
        if ($app ['isEnabled'] != 1) {
            exception('AppId非法');
        }

        // 接口签名认证
        if (config("yfcmf.signature.app_sign_auth_on") === true) {
            $signature = input("signature"); // app端生成的签名
            $param = input("param.");
            unset($param['signature']);
            if (empty($signature)) {
                exception('签名无效');
            }
            //数组排序
            ksort($param);
            $str = http_build_query($param, '', '&', PHP_QUERY_RFC3986);
            if ($signature != md5(sha1($str) . $app['appSecret'])) {
                exception('签名无效');
            }
        }
        return msg_return(1, 'ok');
    }

    /**
     * 验证用户身份
     *
     * @param string $type user 普通用户，admin 管理员，seller 商家
     *
     * @return mixed
     *
     * @throws
     */
    public static function checkUser($type = 'user')
    {
        // JWT用户令牌认证，令牌内容获取
        $userToken = request()->header('x-access-token');
        if (empty($userToken)) {
            exception('token失效');
        }
        $userToken = think_decrypt($userToken);
        $payload = JwtUtil::decode($userToken);
        if ($payload === false || empty($payload->uid) || empty($payload->loginTime)) {
            exception('token失效');
        }
        //用户登录有效期
        $userLoginTime = config('yfcmf.user_login_time');
        if ($payload->loginTime < time() - $userLoginTime) {
            exception('token失效');
        }
        // 实时用户数据
        $user = Db::name($type)->find($payload->uid);
        //是否多设备登录
        if (!empty($user ['loginTime']) && $user ['loginTime'] != $payload->loginTime) {
            exception('禁止多设备登录');
        }
        //认证：状态
        if ($user ['isEnabled'] != 1) {
            exception('用户被禁用');
        }
        return msg_return(1, $user);
    }

}
