<?php
// +----------------------------------------------------------------------
// | YFCMF [ WE CAN DO IT MORE SIMPLE]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2020 http://yfcmf.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: rainfer <rainfer520@qq.com>
// +----------------------------------------------------------------------

use think\facade\Env;
use app\admin\model\setting\Attachment as AttachmentModel;

/**
 * 上传
 * @author rainfer520@qq.com
 *
 */
class UploadUtil
{

    /**
     * 上传图片
     *
     * @param $name
     * @param $path
     *
     * @return mixed
     *
     * @throws
     */
    public static function upImage($name = '', $path = '')
    {

        if (empty($path)) {
            // 框架应用根目录/uploads/ 目录下
            $path = Env::get('root_path') . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images';
        }
        $files = request()->file($name);
        //检测是否存在
        $md5 = $files->hash('md5');
        $name = $files->getInfo('name');
        $res = AttachmentModel::check($md5);
        if (!$res || $res['url'] === false) {
            $info = $files->validate(['size' => 1024 * 1024 * 5, 'ext' => ['gif', 'jpg', 'jpeg', 'png']])->move($path);
            if ($info) {
                // 成功上传后 获取上传信息
                $url = '/uploads/images/' . $info->getSaveName();
                $url = str_replace('\\', '/', $url);
                $data = [
                    'name' => $name,
                    'attDir' => $url,
                    'attSize' => $info->getSize(),
                    'attType' => 'image',
                    'md5' => md5_file('.' . $url),
                ];
                AttachmentModel::create($data);
                return ['url'=>$url,'name'=>$name];
            } else {
                // 上传失败获取错误信息
                exception($files->getError());
            }
        } else {
            return $res;
        }
        return false;
    }


    /**
     * 上传视频
     *
     * @param $name
     * @param $path
     *
     * @return mixed
     *
     * @throws
     */
    public static function upVideo($name = '', $path = '')
    {

        if (empty($path)) {
            // 框架应用根目录/uploads/ 目录下
            $path = Env::get('root_path') . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'video';
        }
        $files = request()->file($name);
        //检测是否存在
        $md5 = $files->hash('md5');
        $name = $files->getInfo('name');
        $res = AttachmentModel::check($md5);
        if (!$res || $res['url'] === false) {
            $info = $files->validate(['size' => 1024 * 1024 * 100, 'ext' => ['mp4']])->move($path);
            if ($info) {
                // 成功上传后 获取上传信息
                $url = '/uploads/video/' . $info->getSaveName();
                $url = str_replace('\\', '/', $url);
                $data = [
                    'name' => $name,
                    'attDir' => $url,
                    'attSize' => $info->getSize(),
                    'attType' => 'video',
                    'md5' => md5_file('.' . $url),
                ];
                AttachmentModel::create($data);
                return ['url'=>$url,'name'=>$name];
            } else {
                // 上传失败获取错误信息
                exception($files->getError());
            }
        } else {
            return $res;
        }
        return false;
    }

    /**
     * 上传文件
     *
     * @param $name
     * @param $path
     *
     * @return mixed
     *
     * @throws
     */
    public static function upFile($name = '', $path = '')
    {

        if (empty($path)) {
            // 框架应用根目录/uploads/ 目录下
            $path = Env::get('root_path') . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'file';
        }
        $files = request()->file($name);
        //检测是否存在
        $md5 = $files->hash('md5');
        $name = $files->getInfo('name');
        $res = AttachmentModel::check($md5);
        if (!$res || $res['url'] === false) {
            $info = $files->validate(['size' => 1024 * 1024 * 5])->move($path);
            if ($info) {
                // 成功上传后 获取上传信息
                $url = '/uploads/file/' . $info->getSaveName();
                $url = str_replace('\\', '/', $url);
                $data = [
                    'name' => $name,
                    'attDir' => $url,
                    'attSize' => $info->getSize(),
                    'attType' => 'file',
                    'md5' => md5_file('.' . $url),
                ];
                AttachmentModel::create($data);
                return ['url'=>$url,'name'=>$name];
            } else {
                // 上传失败获取错误信息
                exception($files->getError());
            }
        } else {
            return $res;
        }
        return false;
    }
}
