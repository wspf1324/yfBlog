<?php
return array (
  'yfblog_version' => 'V1.0.0',
  'jwt'=>[
    'secret_key'=>'dskdiejsksdisfdf',
    'algorithm'=>'HS256'
  ],
  'signature'=>[
    'app_sign_auth_on'=>true
  ],
  'user_login_time'=>7*24*3600,
  'pass_salt'=>'idksodkfdua',
  'verify' =>
  array (
    'fontSize' => 20,
    'fontttf' => '4.ttf',
    'imageH' => 42,
    'imageW' => 250,
    'length' => 5,
    'useCurve' => false,
  ),
  'app_trace' => true,
  'app_debug' => true,
  'route_annotation'=>true,
);
